/*
 Programming 2020 Jan-Dirk van Dingenen 
 Testing: Kenneth YWN, Steven Durst
 Original Game Designer: Jeffrey CCH
 Original Producer and Game developer: Jeffrey CCH and Kenneth YWN
 Original graphics: Vincci lala and Rubens Ma
*/


document.addEventListener("DOMContentLoaded", function(event) {    
    const END = 'game end';
    const INSTANT = 'instant';
    const CHANGE = 'change';
    const GAINED = 'gained';
    const WAR = 'war';
    const TURNEND = 'turnend';
    const RISE = 'rise';
    const ANNEX = 'annex';
    const REST = 'rest';
    const FUTURE = 'future rounds';
    
    const SACRIFICE = 'sacrifice';
    const FREEACTION = 'free action';
    const NEWACTION = 'add new action';    
    const WORKERCOST = 'workerCost';
    const COINCOST = 'coinCost';
    const SPECIAL = 'special ability';
    const PLAGUE = 'plague';
    
    const WORKER = 'worker';
    const VP = 'vp';
    const COIN = 'coins';
    const SCIENCE = 'science';
    const SHIELD = 'shield';
    const WONDER = 'wonder';
    
    const MODE_RISE = 'modeRise';
    const MODE_ANNEX = 'modeAnnex';
    const MODE_WONDER = 'modeWonder';
    const MODE_SCIENCE = 'modeScience';
    const MODE_WORKERS = 'modeWorkers';
    const MODE_FREEACTION = 'modeFreeAction';
    const MODE_WAIT = 'modeWait';
    const MODE_ENDROUND = 'modeEndRound';
    const MODE_REST = 'modeRest';
    const MODE_GAMEOVER = 'modeGameover';
    
    const INFO = 'info';
    const ERROR = 'error';
    const WARN = 'warning'; 
    const ACHIEVEMENT = 'achievement';

    class Util {
        constructor() {
            this.genericId = 1000;
            this.screenHeight = 1;
            this.screenWidth = 1;
            this.baseUnit = 100;
            this.cardHeight = 151;
            
            this.sound;
            this.initSound();
            
            this.wait = 800;

            this.initShortCuts();
            this.initPlayerName();
            this.setPlayerName(); 
            this.resize();
            
            this.achievements = [];
            
            this.holdTimeout = null;
            
            let self = this;
            window.onresize = function(){
                if(!self.isMobile()) {
                    self.resize();
                }
            };
        }
        
        initSound() {
            this.sound = new Howl({
                src: ['aoc.mp3'], 
                sprite: {
                    coin: [0, 500],    
                    sword: [500, 500],
                    trumpet: [1000, 600],
                    click:[1700,100]
                }
            });
            Howler.volume(0.5); 
        }
        
        playMultiSound(soundName, amount) {
            for(let s=0; s < amount; s++) {
                setTimeout(function(){
                    util.sound.play(soundName);
                },s*250);
            }
        }
        
        initAchievements() {                        
            this.playerAchievements = this.useLocalStorage('aocAch',[false,false,false,false,false,false,false,false],true, true);
            new Achievement(1,'Remarkable Clan','Score 20 or more points',[[VP,">=",20]]);
            new Achievement(2,'Great Kingdom','Score 25 or more points',[[VP,">=",25]]);
            new Achievement(3,'Mighty Empire','Score 30 or more points',[[VP,">=",30]]);
            new Achievement(4,'Eternal Civilization','Score 35 or more points',[[VP,">=",35]]);
            new Achievement(5,'Realm of Technology','Score 25 or more points with 9 technologies',[[VP,">=",25],[SCIENCE,">=",9]]);
            new Achievement(6,'Realm of Primitives','Score 25 or more points with no technologies',[[VP,">=",25],[SCIENCE,"<=",0]]);
            new Achievement(7,'World of Wonders','Score 25 or more points with 4 wonders',[[VP,">=",25],[WONDER,">=",4]]);
            new Achievement(8,'Super Power','Score 25 or more points with 3 or less technologies and at least 9 military strength',[[VP,">=",25],[SCIENCE,"<=",3],[SHIELD,">=",9]]);
        }
        
        saveAchievements() {
            let newAchievementList = [];
            for(let ach of this.achievements) {
                if(ach.achieved) {
                    newAchievementList.push(ach.when);
                }
                else {
                    newAchievementList.push(false);
                }
            }
            this.useLocalStorage('aocAch',JSON.stringify(newAchievementList));
        }
        
        checkAchievements() {
            let gainedAchievement = false;
            for(let ach of this.achievements) {
                if(!ach.achieved) {
                    let conditionsFullfilled = true;
                    for(let req of ach.requirements) {
                        let what = req[0];
                        let operator = req[1];
                        let amount = req[2];                        
                        if(operator === '>=') {
                            if(game.player.resources[what] < amount) {
                                conditionsFullfilled = false;
                            }
                        }
                        if(operator === '<=') {
                            if(game.player.resources[what] > amount) {
                                conditionsFullfilled = false;
                            }
                        }
                    }
                    if(conditionsFullfilled) {
                        ach.achieved = true;
                        ach.when = new Date().toLocaleDateString();
                        game.board.toast('New Achievement<br/>'+ach.name,ACHIEVEMENT);
                        gainedAchievement = true;
                    }                    
                }
            }     
            if(gainedAchievement) {
                this.saveAchievements();
            }
        }
        
        resize() {
            let baseHeightUnits = 19;
            let baseWidthUnits = 35;
            this.screenHeight = window.innerHeight;
            this.screenWidth = window.innerWidth;            
            if(this.screenWidth / baseWidthUnits < this.screenHeight / baseHeightUnits) {
                this.baseUnit = Math.floor(this.screenWidth/baseWidthUnits);
            }
            else {
                this.baseUnit = Math.floor(this.screenHeight/baseHeightUnits);
            }
            
            this.cardWidth = this.baseUnit * 3;
            this.cardHeight = this.baseUnit * 4;
            
            qTag("body")[0].style.setProperty('--baseUnit', this.baseUnit+'px');  
        }
        
        initShortCuts() {
            q = document.querySelector.bind(document);
            qAll = document.querySelectorAll.bind(document);
            qId = document.getElementById.bind(document);
            qClass = document.getElementsByClassName.bind(document);
            qTag = document.getElementsByTagName.bind(document);
        }

        cloneObject(myObject) {
            return JSON.parse(JSON.stringify(myObject));
        }

        shuffle(myList) {
            var j, x, i;
            for (i = myList.length; i; i -= 1) {
                j = Math.floor(Math.random() * i);
                x = myList[i - 1];
                myList[i - 1] = myList[j];
                myList[j] = x;
            }    
            return myList;
         }

        getId() {
            this.genericId++;
            return this.genericId;
        }  

        useLocalStorage(varName, value, get, parseValue) {
            var hasLocalStorage;
            try {
                hasLocalStorage = 'localStorage' in window && window['localStorage'] !== null;
             }
            catch (e) {
                return value;
            }
            var returnValue = value;
            if(hasLocalStorage) {
                if(get) {
                    var returnValue = localStorage[varName];
                    if(typeof returnValue !== "undefined") {
                        try {
                            returnValue = JSON.parse(returnValue);
                        }
                        catch(e2) {}
                    }
                    else {
                        returnValue = value;
                    }
                }
                else {
                    localStorage[varName] = value;
                }
            }
            return returnValue;
        }

        addTextToNode(node,text,replace) {
            if(node) {
                if(replace) {
                    node.innerHTML = '';
                }
                var newContent = document.createTextNode(text); 
                node.appendChild(newContent);
            }
        }

        xhr(url, data, callback, errorCallback) {            
            var request = new XMLHttpRequest();
            
            var protocol = 'POST';
            if(!data) {
                data = null;
                protocol = 'GET';
            }
            request.onreadystatechange = function(){
                if (request.readyState === 4){
                    if(callback) {
                        callback(request.responseText);
                    }
                }
            };
            request.onerror = function(){
                console.log('error',request);
                if(errorCallback) {
                    errorCallback(request);
                } 
            };
            request.open(protocol, url);
            request.send(data);
        }
        
        createElement(elementType, classList, content) {
            let newElement = document.createElement(elementType);
            newElement.classList.add(...classList);
            if(content) {
                newElement.innerHTML = content;
            }
            return newElement;
        }
        
        initPlayerName() {
            this.addTextToNode(q('#playerName'), playerName,true);
            let self = this;
            q('#playerName').addEventListener('click',function(){
                self.getPlayerName();
            });  
        }
        
        getPlayerName() {
            playerName = prompt('Leader name ?');
            if(!playerName || playerName.trim() === '') {
                playerName = 'Player'+Math.floor(Math.random()*10000);
            }
            playerName = playerName.substring(0,16);
            this.useLocalStorage('aocName', playerName);     
            this.addTextToNode(q('#playerName'), playerName,true);
            if(menu) {
                menu.initScores(true);
            }
        }

        setPlayerName() {
            playerName = this.useLocalStorage('aocName', null,true);            

            if(!playerName) {
                let self = this;
                setTimeout(function(){
                    self.getPlayerName();
                },10);
            }
            else {
                playerName = playerName.substring(0,16);
                let playerNode = q('#playerName');
                this.addTextToNode(playerNode,playerName,true);
                if(menu) {
                    menu.initScores(true);
                }
            }
        }
        
        isMobile() {
            //from http://detectmobilebrowsers.com/
            var check = false;
            (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|android|playbook|silk|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
            return check;
        }
        
        randWait() {
            return (Math.random()*this.wait*0.5) + this.wait*0.5;
        }
    }
    
    class Game {
        constructor() {
            this.allActions = [];       //list of all actions
            this.allWonders = [];       //list of all wonders
            this.allCivs = [];          //list of all civs
            this.allBonusses = [];      //list of all bonusses
            this.inventions = [];       //list of all inventions
            this.civDeck = [];          //list of all unused civs
            
            this.actions = [];          //list of action cards, available actions are kept at player         
            this.wonders = [];          //All available wonders
            this.civs = [];             //All available civs      
                                                           
            this.round = 0;
            this.mode = MODE_WAIT;
            
            this.dynamicBonusses = new Set();
            this.history = [];
        }
        
        init() {
            this.player = new Player('Player');
            this.opponent = new Player('#Opponent'); 
            this.createActions();
            this.createWonders();
            this.createInventions();
            this.createCivs();                                     
        }
        
        start() {
            let useAdditionalCivs = qId('unofficial').checked;
            util.useLocalStorage('aocMoreCivs',useAdditionalCivs);
            if(!useAdditionalCivs) {
                this.civDeck = this.civDeck.filter(c => c.unofficial !== true);
            }            
            this.civs = this.civDeck.splice(0,5);            
            
            this.board = new Board();   
            qId('gameWrapper').classList.add('fadeIn');    
            qId('playerName').style.display = 'none';
            this.initEvents();            
            this.board.updateInfo();
            this.board.inventionReachable();          
            qTag("body")[0].style.setProperty('--round', this.round);
            this.phase1();
        }
        
        initEvents() {
            qId('info').addEventListener('click',function(){
                util.sound.play('click');
                if(this.parentElement.classList.contains('open')) {
                    this.parentElement.classList.remove('open');
                }
                else {
                    this.parentElement.classList.add('open');
                }
            });
            
            for(let el of qAll('.card')) {
                el.addEventListener("touchstart", function(event) {
                    event.stopPropagation();
                    el.focus();
                });
            }
            
            qId('game').addEventListener("touchstart", function(event) {
                document.activeElement.blur();
            });
            
            qId('endTurn').addEventListener('click',function() {
                util.sound.play('click');
                game.player.workersPlaced += game.player.freeWorkers;
                game.endRound();
            });
            
            for(let el of qAll('#science .tech')) {
                let thisInvention = game.getInventionById(parseInt(el.getAttribute('data-tech')));
                el.addEventListener("click", function(){
                    thisInvention.clicked();
                });
            }
            
            document.addEventListener('mouseup',function(){
                clearTimeout(util.holdTimeout);
                let enlargedCards = qAll('.enlarge');
                for(let enlargedCard of enlargedCards) {
                    enlargedCard.classList.remove('enlarge');
                }
            });
        }
       
        createActions() {
            this.actions.push(new Action(1,"Culture",4,1,false,[new Bonus(INSTANT,VP,2)],false));
            this.actions.push(new Action(2,"Build",2,2,false,[new Bonus(INSTANT,VP,2)],false));
            this.actions.push(new Action(3,"Conquest",0,1,true,[new Bonus(INSTANT,VP,2,null,null,null,null,4)],true));
            this.actions.push(new Action(4,"Hunt",0,1,false,[new Bonus(INSTANT,COIN,1), new Bonus(INSTANT,VP,1,null,null,null,null,1)],true));
            let plagueAction = new Action(5,"Plague",0,1,true,[new Bonus(INSTANT,WORKER,-1)],PLAGUE);
            this.actions.push(new Action(6,"Fish",0,1,false,[new Bonus(INSTANT,COIN,1)],true));
            this.actions.push(new Action(7,"Trade",0,1,false,[new Bonus(INSTANT,COIN,2)],true));
            this.actions.push(new Action(8,"Farm",0,2,false,[new Bonus(INSTANT,COIN,3)],false));
            
            this.actions = util.shuffle(this.actions);
            this.actions.splice(4,0,plagueAction);
            
            let action9 = new Action(9,"Wonder",7,1,true,[new Bonus(INSTANT,VP,3),new Bonus(INSTANT,WONDER,1)]);
            let action10 = new Action(10,"Research",4,1,false,[new Bonus(INSTANT,VP,1),new Bonus(INSTANT,SCIENCE,1)]);
            let action11 = new Action(11,"Exploit",0,1,true,[new Bonus(INSTANT,COIN,2)]);
            action9.addClickEvent();
            action10.addClickEvent();
            action11.addClickEvent();
        }
        
        createWonders() {
            new Wonder(1,"Parthenon",[new Bonus(END,VP,2)]);
            new Wonder(2,"Great Pyramid",[new Bonus(END,VP,1,null,WONDER)]);
            new Wonder(3,"Great Library",[new Bonus(END,VP,0.5,null,SCIENCE)]);
            new Wonder(4,"Great Colosseum",[new Bonus(END,VP,0.34,null,SHIELD)]);
            new Wonder(5,"Hanging Garden",[new Bonus(CHANGE,VP,1,1)]);
            new Wonder(6,"Machu Pichu",[new Bonus(CHANGE,VP,1,2)]);
            new Wonder(7,"Great Lighthouse",[new Bonus(CHANGE,VP,1,6)]);
            new Wonder(8,"Chichen Itzen",[new Bonus(INSTANT,VP,1),new Bonus(CHANGE,VP,1,8)]);
            new Wonder(9,"Ankor Wat",[new Bonus(INSTANT,VP,4,null,null,COIN,3,null)]);
            new Wonder(10,"Stonehenge",[new Bonus(INSTANT,VP,2,null,null,null,null,1)]);
            new Wonder(11,"Terracotta Army",[new Bonus(INSTANT,VP,2,null,null,null,null,2)]);
            new Wonder(12,"Petra",[new Bonus(TURNEND,VP,1,null,null,null,null,3)]);
            new Wonder(13,"Himeji Castle",[new Bonus(INSTANT,VP,1),new Bonus(WAR,VP,1,null,null,null,null,4)]);
            new Wonder(14,"Great Wall",[new Bonus(END,VP,0.34,null,SHIELD,null,null,null,true)]);
            new Wonder(15,"Colossus",[new Bonus(END,VP,2,null,null,null,null,5)]);
            
            this.allWonders = util.shuffle(this.allWonders);
            this.wonders = this.allWonders.splice(0,4);
        }
        
        createInventions() {
            new Invention(1,"Science",[new Bonus(INSTANT,VP,1,null,FUTURE)],[]);
            new Invention(2,"Engineering",[new Bonus(END,VP,1,null,WONDER),new Bonus(CHANGE,VP,1,2)],[]);
            new Invention(3,"Gunpowder",[new Bonus(INSTANT,SHIELD,3),new Bonus(CHANGE,VP,1,3)],[]);
            new Invention(4,"Education",[new Bonus(TURNEND,FREEACTION,1)],[1,2]);
            new Invention(5,"Mathematics",[new Bonus(CHANGE,COINCOST,-1,10)],[1,2,3]);
            new Invention(6,"Iron Working",[new Bonus(INSTANT,SHIELD,3)],[2,3]);
            new Invention(7,"Writing",[new Bonus(CHANGE,VP,1,1)],[4]);
            new Invention(8,"Sailing",[new Bonus(INSTANT,SHIELD,1), new Bonus(CHANGE,COIN,1,6), new Bonus(CHANGE,COIN,1,7)],[5]);
            new Invention(9,"Mining",[new Bonus(INSTANT,SHIELD,2), new Bonus(CHANGE,COINCOST,-1,2)],[6]); 
        }
        
        createCivs() {
            new Civilisation(1,'Egypt',4,[new Bonus(CHANGE,COINCOST,-1,9),new Bonus(INSTANT,SHIELD,1)],[new Bonus(CHANGE,COINCOST,-1,2)]);
            new Civilisation(2,'India',4,[new Bonus(CHANGE,NEWACTION,1)],[new Bonus(CHANGE,COIN,1,8)]); 
            new Civilisation(3,'Rome',4,[new Bonus(INSTANT,SHIELD,0.5,null,SCIENCE,null,null,null,null,true)],[new Bonus(INSTANT,SHIELD,2)]);
            new Civilisation(4,'Persia',3,[new Bonus(WAR,COIN,0.5,null,SHIELD)],[new Bonus(INSTANT,SHIELD,2)]);
            new Civilisation(5,'Arabia',3,[new Bonus(TURNEND,COIN,0.5,null,SCIENCE)],[new Bonus(CHANGE,COIN,1,7)]);
            new Civilisation(6,'Huns',3,[new Bonus(CHANGE,COIN,2,4)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(9,'Hittites',3,[new Bonus(INSTANT,SPECIAL,1),new Bonus(INSTANT,SHIELD,1)],[new Bonus(CHANGE,COIN,2,3)]); // SPECIAL 1 : Hittites can research Iron Working for 0 COIN
            new Civilisation(11,'Aztecs',3,[new Bonus(CHANGE,COINCOST,-6,9),new Bonus(CHANGE,WORKERCOST,1,9)],[new Bonus(CHANGE,NEWACTION,8)]);
            new Civilisation(12,'Greece',2,[new Bonus(CHANGE,COINCOST,-2,10), new Bonus(INSTANT,SHIELD,2)],[new Bonus(CHANGE,VP,1,1)]);
            new Civilisation(13,'Babylon',2,[new Bonus(TURNEND,SCIENCE,1)],[new Bonus(CHANGE,COINCOST,-1,10)]);
            new Civilisation(14,'Japan',2,[new Bonus(INSTANT,SHIELD,1), new Bonus(CHANGE,COINCOST,-0.5,1,SHIELD,null,null,null,null,true)],[new Bonus(CHANGE,COIN,1,6)]);
            new Civilisation(15,'Inca',3,[new Bonus(RISE,WONDER,1)],[new Bonus(CHANGE,COINCOST,-1,2)]);
            new Civilisation(16,'Khmer',3,[new Bonus(RISE,COIN,3,null,WONDER)],[new Bonus(CHANGE,VP,1,9)]);
            new Civilisation(17,'Byzantium',4,[new Bonus(CHANGE,COINCOST,-1,1,null,null,null,4,null,true)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(18,'Celts',3,[new Bonus(CHANGE,VP,3,1,null,null,null,1)],[new Bonus(CHANGE,NEWACTION,4)]);
            new Civilisation(19,'Sumer',2,[new Bonus(INSTANT,SPECIAL,2)],[new Bonus(CHANGE,WORKERCOST,-1, 8)]);             // SPECIAL 2 : Sumer can research Writing and Sailing for 0 COIN
            new Civilisation(20,'Ottoman',3,[new Bonus(INSTANT,SPECIAL,3)],[new Bonus(CHANGE,VP,1,3)]);                     // SPECIAL 3 : Ottoman can research Science and Gunpowder for 3 COINS less
            new Civilisation(21,'Polynesia',3,[new Bonus(INSTANT,SPECIAL,4), new Bonus(CHANGE,COIN,1,6)],[new Bonus(CHANGE,NEWACTION,6)]);                // SPECIAL 4 : Polynesia does not lose a WORKER when losing a WAR
            new Civilisation(22,'Indonesia',4,[new Bonus(CHANGE,NEWACTION,2)],[new Bonus(CHANGE,COIN,1,6)]);
            new Civilisation(23,'Assyria',3,[new Bonus(WAR,WORKER,1,null,null,null,null,6), new Bonus(WAR,COIN,1,null,null,null,null,6)],[new Bonus(CHANGE,COIN,2,3)]);
            new Civilisation(24,'Korea',2,[new Bonus(INSTANT,SPECIAL,5)],[new Bonus(CHANGE,NEWACTION,6)]);                // SPECIAL 5 : Korea gains 2 VP when researching Writing, Education or Sailing
            new Civilisation(25,'Maya',3,[new Bonus(INSTANT,SPECIAL,6),new Bonus(CHANGE,COIN,1,8)],[new Bonus(CHANGE,COINCOST,-1,9)]);    // SPECIAL 6 : Maya may take future actions
            new Civilisation(26,'Nubia',3,[new Bonus(CHANGE,WORKERCOST,-1,2)],[new Bonus(END,VP,1,null,WONDER)]);
            new Civilisation(27,'Mali',3,[new Bonus(CHANGE,COIN,2,7)],[new Bonus(CHANGE,NEWACTION,2)]);
            new Civilisation(28,'Siam',4,[new Bonus(CHANGE,COIN,1,8)],[new Bonus(END,VP,0.5,null,WORKER)]);
            new Civilisation(30,'Italy',2,[new Bonus(RISE,COIN,2),new Bonus(CHANGE,VP,1,1)],[new Bonus(END,VP,0.5,null,SCIENCE)]);
            new Civilisation(31,'Britain',2,[new Bonus(TURNEND,VP,1.01/7,null,VP)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(32,'Francia',3,[new Bonus(CHANGE,NEWACTION,3),new Bonus(CHANGE,VP,1,3)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(33,'Kongo',3,[new Bonus(REST,COIN,1),new Bonus(REST,VP,2)],[new Bonus(CHANGE,NEWACTION,4)]);
            new Civilisation(34,'Slavs',4,[new Bonus(TURNEND,COIN,1,null,null,null,null,4)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(35,'Harappa',4,[new Bonus(INSTANT,SPECIAL,7)],[new Bonus(CHANGE,COINCOST,-1,1,null,null,7)]);         // SPECIAL 7 : Harappa can research Writing, Sailing and Mining for 1 COINS less
            new Civilisation(36,'Berber',2,[new Bonus(CHANGE,NEWACTION,7),new Bonus(CHANGE,COIN,1,7),new Bonus(CHANGE,VP,1,7)],[new Bonus(TURNEND,COIN,1)]);
            new Civilisation(37,'Tiwanaku',2,[new Bonus(RISE,VP,1,null,FUTURE)],[new Bonus(CHANGE,NEWACTION,2)]);
            new Civilisation(39,'Khitan',3,[new Bonus(INSTANT,SHIELD,1,null,WORKER,null,null,null,null,true)],[new Bonus(END,VP,0.5,null,WORKER)]);
            new Civilisation(40,'Vietnam',3,[new Bonus(CHANGE,NEWACTION,6),new Bonus(CHANGE,NEWACTION,2)],[new Bonus(CHANGE,NEWACTION,8)]);
            new Civilisation(42,'Minoan',2,[new Bonus(TURNEND,VP,2,null,null,null,null,8)],[new Bonus(END,VP,0.5,null,SCIENCE)]);
            new Civilisation(43,'Lydia',3,[new Bonus(RISE,COIN,4)],[new Bonus(CHANGE,COIN,1,7)]);
            new Civilisation(44,'Malay',4,[new Bonus(RISE,VP,1,null,null,null,null,9)],[new Bonus(CHANGE,NEWACTION,6)]); 
            new Civilisation(46,'Aksum',3,[new Bonus(CHANGE,VP,1,2),new Bonus(INSTANT,SPECIAL,8)],[new Bonus(CHANGE,COIN,1,8)]); // SPECIAL 8 : Aksum when you build, you may do culture without workers
            new Civilisation(47,'China',4,[new Bonus(CHANGE,WORKER,1,8)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(48,'Dutch',2,[new Bonus(CHANGE,COIN,2,7),new Bonus(INSTANT,SHIELD,0.34,null,COIN,null,null,null,null,true)],[new Bonus(CHANGE,NEWACTION,7)],true);
            new Civilisation(49,'Gaul',3,[new Bonus(CHANGE,VP,2,3,null,null,null,1), new Bonus(INSTANT,SHIELD,1)],[new Bonus(INSTANT,SHIELD,1)]);
            new Civilisation(50,'Xiongnu',3,[new Bonus(CHANGE,SACRIFICE,-1,3)],[new Bonus(INSTANT,SHIELD,3)],true);
            new Civilisation(51,'Caral',3,[new Bonus(CHANGE,WORKERCOST,-1,2,null,null,null,8,null,true), new Bonus(CHANGE,COIN,1,6)],[new Bonus(CHANGE,NEWACTION,2)],true);
            new Civilisation(52,'Chola',4,[new Bonus(CHANGE,VP,1,7)],[new Bonus(CHANGE,VP,1,3)],true);
            new Civilisation(53,'Benin',3,[new Bonus(RISE,SPECIAL,9)],[new Bonus(CHANGE,NEWACTION,2)],true);  // When Benin rises every 4 coins is exchanged for 1 vp
            new Civilisation(54,'Zulu',4,[new Bonus(TURNEND,SHIELD,1)],[new Bonus(CHANGE,VP,1,7)],true);
            new Civilisation(55,'Portugal',3,[new Bonus(INSTANT,SPECIAL,11)],[new Bonus(CHANGE,NEWACTION,7)],true);   // SPECIAL 11 : Portugal, Trade twice in a turn to gain 1 worker
            new Civilisation(56,'Anisazi',3,[new Bonus(CHANGE,VP,3,2,null,null,null,1)],[new Bonus(CHANGE,NEWACTION,9)],true);
            new Civilisation(57,'Parthia',4,[new Bonus(INSTANT,SPECIAL,10)],[new Bonus(CHANGE,VP,1,3)],true);      // SPECIAL 10: Parthia gains 2 VP when annexing a civilization 
            new Civilisation(58,'Akkad',4,[new Bonus(INSTANT,SPECIAL,12)],[new Bonus(END,SPECIAL,13)],true);  // SPECIAL 12: Akkad gain 1 coin when losing a worker , SPECIAL 13: 3 VP if no workers left at end of game 
            
            this.civDeck = util.shuffle(this.civDeck);     
        }
                
        checkSpecialCondition(conditionId) {        
            switch(conditionId) {
                case 1:     // no science
                    if(this.player.resources[SCIENCE] > 0) {
                        return false;
                    }
                    break
                case 2:     // 4 workers
                    if(this.player.resources[WORKER] !== 4) {
                        return false;
                    }
                    break
                case 3:     // 5 or more coins
                    if(this.player.resources[COIN] < 5) {
                        return false;
                    }
                    break
                case 4:     // most shields
                    if(this.player.resources[SHIELD] < this.opponent.resources[SHIELD] && this.player.resources[SHIELD] > 0) {
                        return false;
                    }
                    break
                case 5:     // invented Engineering
                    if(this.player.inventions.findIndex(i => i.id === 2) === -1) {
                        return false;
                    }
                    break
                case 6:     // not least shields
                    if(this.player.resources[SHIELD] <= this.opponent.resources[SHIELD]) {
                        return false;
                    }
                    break
                case 7:    // action used us not the culture from Education                    
                    break;
                case 8:     // no shields
                    if(this.player.resources[SHIELD] > 0) {
                        return false;
                    }
                    break
                 case 9:     // invented Sailing
                    if(this.player.inventions.findIndex(i => i.id === 8) === -1) {
                        return false;
                    }
                    break
            }
            
            return true;
        }
        
        getActionById(actionId) {
            return this.allActions.find(a => a.id === actionId);
        }
        
        getInventionById(inventionId) {
            return this.inventions.find(i => i.id === inventionId);
        }
        
        getCivById(civId) {
            return this.allCivs.find(c => c.id === civId);
        }
        
        endRound() {
            if(this.mode === MODE_WORKERS || this.mode === MODE_REST) {
                this.changeMode(MODE_ENDROUND);
                game.player.tradeCount = 0; // SPECIAL 11 : Portugal, Trade twice in a turn to gain 1 worker
                this.player.checkBonusList(game.player.bonusses,TURNEND,null,function(){
                    for(let civ of game.civs) {
                        if(civ.available) {
                            civ.addCoin(1);
                        }
                    }
                    game.board.updateCoinsOnCivs();
                    game.civDeck = util.shuffle(game.civDeck);
                    let newCiv = game.civDeck.splice(0,1)[0];
                    game.civs.push(newCiv);
                    qId('civs').append(newCiv.show());
                    
                    game.player.resources[WORKER] = game.player.workersPlaced;
                    if(game.player.resources[WORKER] > 4) {
                        game.player.resources[WORKER] = 4;
                    }
                    game.board.removeAllWorkers();

                    setTimeout(function(){
                        game.board.updateInfo();
                        game.nextRound();
                    },util.wait*2.1);  
                });          
            }
        }
        
        changeMode(newMode) {
            let oldMode = this.mode;
            let modeMessage = qId('modeMessage');
            this.mode = newMode;            
            
            qId('game').setAttribute("class", newMode);
            let leftBarWasOpen = qId('leftBar').classList.contains('open');
            qId('leftBar').setAttribute("class", newMode);
            if(leftBarWasOpen) {
                qId('leftBar').classList.add('open');
            }
            
            switch(newMode) {
                case MODE_FREEACTION:
                    modeMessage.innerHTML = 'Do a free action';
                    break;
                case MODE_RISE:
                    this.board.hideMessage();
                    modeMessage.innerHTML = 'Rise a Civilization';
                    break;
                case MODE_ANNEX:
                    this.board.hideMessage();
                    modeMessage.innerHTML = 'Annex a Civilization';
                    break;
                case MODE_WORKERS:
                    this.board.hideMessage();
                    modeMessage.innerHTML = 'Assign workers to Actions';
                    break;
                case MODE_SCIENCE:
                    this.board.hideMessage();
                    modeMessage.innerHTML = 'Research a new Technology';
                    qId('leftBar').classList.add('open');
                    break;  
                case MODE_WONDER:
                    this.board.hideMessage();
                    modeMessage.innerHTML = 'Build a Wonder';
                    break; 
                case MODE_REST:
                    this.board.hideMessage();
                    modeMessage.innerHTML = 'Resting...';
                    break; 
                case MODE_ENDROUND:
                    modeMessage.innerHTML = 'End of Round';
                    break;     
                case MODE_WAIT:
                    modeMessage.innerHTML = '';
                    break;
            }
            console.log('mode',this.mode);
        }
        
        phase1() {            
            let buttons = [];
            let message = 'Phase 1: Rise or Annex a civilization.';                       
                        
            if(this.round !== 0) {
                if(this.player.civs.length < 3) {
                    let riseButton = document.createElement('button'); 
                    riseButton.innerHTML = 'Rise';
                    riseButton.addEventListener('click',function(){
                        util.sound.play('click');
                        game.changeMode(MODE_RISE);
                    });
                    buttons.push(riseButton);

                    let annexButton = document.createElement('button');           
                    annexButton.innerHTML = 'Annex';
                    annexButton.addEventListener('click',function(){
                        util.sound.play('click');
                        game.changeMode(MODE_ANNEX);
                    });
                    buttons.push(annexButton);

                    let skipButton = document.createElement('button');   
                    skipButton.innerHTML = 'Skip';
                    skipButton.addEventListener('click',function(){
                        util.sound.play('click');
                        game.addHistory('Skip','');
                        game.phase2();
                    });
                    buttons.push(skipButton);
                    modeMessage.innerHTML = 'Rise or Annex a Civilization';
                    this.board.showMessage(message,buttons);
                }
                else {
                    game.addHistory('Skip','');
                    game.phase2();
                }
            }
            else {
                this.changeMode(MODE_RISE);
                message = 'Phase 1: Select a civilization to rise.';  
                modeMessage.innerHTML = 'Rise a Civilization';
                this.board.showMessage(message,buttons);
            }                                            
        }
        
        phase2() {
            let self = this;
            let buttons = [];
            let message = 'Phase 2: Take actions or Rest.'; 
            
            let actionsButton = document.createElement('button');           
            actionsButton.innerHTML = 'Take Actions';
            actionsButton.addEventListener('click',function(){      
                util.sound.play('click');
                self.player.freeWorkers = self.player.resources[WORKER];
                self.player.workersPlaced = 0;
                self.changeMode(MODE_WORKERS);
                self.player.resetActions();
                self.board.updateInfo();
            });
            buttons.push(actionsButton);

            let restButton = document.createElement('button');               
            let restHTML = 'Rest and Gain<br/>';
            let restBonusses = this.player.bonusses.filter(b => b.when === REST);
            for(let bonus of restBonusses) {
                for(let amount=0; amount < bonus.amount; amount++) {
                    restHTML += '<div class="icon icon_'+bonus.what+'"></div>';
                }
            }
            
            restButton.innerHTML = restHTML;
            restButton.addEventListener('click',function(){
                util.sound.play('click');
                self.changeMode(MODE_REST);
                self.rest();
                self.addHistory('Rest','');
            });
            buttons.push(restButton);
            this.board.showMessage(message,buttons);
            this.board.updateInfo();
        }
        
        rest() {
            game.player.checkBonusList(game.player.bonusses,REST,null,function(){
                setTimeout(function(){
                    game.player.freeWorkers = game.player.resources[WORKER];
                    game.player.workersPlaced = game.player.resources[WORKER];
                    game.endRound();
                },util.wait);  
            });             
        }
        
        nextRound() {
            this.opponent.addResource(SHIELD,1,null);
            this.round++;
            if(this.round < 6) {
                qTag("body")[0].style.setProperty('--round', this.round);
                this.warAndPlague();                
            }
            else {
                this.endGame();
            }
        }
        
        warAndPlague() {
           let newAction = this.actions[this.round+2];
           if(newAction.war) {
                if(newAction.war === PLAGUE) {
                   let workerLost = true;
                   this.player.resources[WORKER]--;                   
                   if(this.player.resources[WORKER] < 0) {
                       this.player.resources[WORKER] = 0;
                       let workerLost = false;
                   }
                   if(this.player.freeWorkers > this.player.resources[WORKER]) {
                       this.player.freeWorkers = this.player.resources[WORKER];
                   }
                   this.board.updateInfo();
                   if(workerLost) {
                        this.board.toast('<div class="plagueMessage"><div class="plagueTitle">Plague</div><br/><img src="img/plague.png"/><br/><br/>One worker has died !</div>',WARN);
                        if(game.player.special === 12) {
                           game.player.addResource(COIN, 1,'civ_58');
                        }
                   }
                   setTimeout(function(){
                       game.phase1();
                   },util.wait/2);
                }
                else {
                    let workerLost = true;
                    let warMessage = '<div class="warTitle">War</div><div class="warMessage"><div>'+playerName+'</div><div></div><div>&nbsp;<div class="icon icon_opponent"></div></div>';
                    warMessage += '<div><div class="icon icon_shield largeIcon">'+game.player.resources[SHIELD]+'</div></div><div><div class="icon icon_war largeIcon"></div></div><div><div class="icon icon_shield largeIcon">'+game.opponent.resources[SHIELD]+'</div></div></div>';
                    if(this.player.resources[SHIELD] > game.opponent.resources[SHIELD] || this.player.special === 4) {  // special 4 : Polynesia does not lose a WORKER when losing a WAR
                        workerLost = false;
                        this.board.toast(warMessage+'No workers were lost.',INFO);
                    }
                    else {
                        this.player.resources[WORKER]--;
                        if(this.player.resources[WORKER] < 0) {
                            this.player.resources[WORKER] = 0;
                            let workerLost = false;
                        }
                        if(this.player.freeWorkers > this.player.resources[WORKER]) {
                            this.player.freeWorkers = this.player.resources[WORKER];
                        }
                        game.board.updateInfo();
                        if(workerLost) {
                            this.board.toast(warMessage+'One worker was killed !',WARN);
                            if(game.player.special === 12) {
                                game.player.addResource(COIN, 1,'civ_58');
                            }
                        }
                    }
                    game.player.checkBonusList(game.player.bonusses, WAR, 'action_'+newAction.id, function(){
                        game.phase1();                
                    });  
                }
            }
            else {
                this.phase1();
            }
        }
        
        reCalculateDynamicBonusses() {
            for(let bonus of this.dynamicBonusses) {
                if(this.player.bonusses.includes(bonus)) { 
                    if(bonus.dynamicOnAction &&(bonus.what === COINCOST ||bonus.what === WORKERCOST)) {
                        //Japan, Byzantium
                        let action = this.getActionById(bonus.dynamicOnAction);
                        action[bonus.what] -= bonus.dynamicBonusValue;
                        // recalculate the new bonus
                        if(!bonus.condition || (bonus.condition && game.checkSpecialCondition(bonus.condition))) {
                            if(bonus.per) {
                                bonus.dynamicBonusValue = Math.ceil(game.player.resources[bonus.per] * bonus.dynamicAmount);
                            }
                            else {
                                bonus.dynamicBonusValue = bonus.dynamicAmount;
                            }
                            bonus.amount = bonus.dynamicBonusValue;
                            action[bonus.what] += bonus.dynamicBonusValue;
                        }
                        else {
                            bonus.dynamicBonusValue = 0;
                            bonus.amount = 0;
                        }
                    } 
                   else {
                        //Khitan, Rome                            
                        this.player.resources[bonus.what] -= bonus.dynamicBonusValue;
                        // recalculate the new bonus
                        bonus.dynamicBonusValue = Math.floor(game.player.resources[bonus.per] * bonus.dynamicAmount);
                        bonus.amount = bonus.dynamicBonusValue;
                        this.player.resources[bonus.what] += bonus.dynamicBonusValue;
                    }
                }
            }
        }
        
        addHistory(type,value) {
            this.history.push([type,value,new Date().toLocaleTimeString()]);            
        }
        
        endGame() {
            this.changeMode(MODE_GAMEOVER);
            this.player.checkBonusList(game.player.bonusses,END,null,function(){
                setTimeout(function(){
                    console.log('Game log:',game.history);
                    let civs = '';
                    for(let civ of game.player.civs) {
                        if(civs !== '') {
                            civs +=',';
                        }
                        civs += civ.id;
                    }
                    let data = new FormData();
                    data.set("player",playerName);
                    data.set("score",parseInt(game.player.resources[VP]));
                    data.set("history",JSON.stringify(game.history));
                    data.set("civs",civs);
                    util.xhr('//boardwebgames.com/aoc/server.php', data);
                    game.board.toast('GAME OVER<br/><br/>Final score: '+game.player.resources[VP]+'<div class="icon icon_vp"></div>',MODE_GAMEOVER);
                    util.checkAchievements();
                    let reloadButton = util.createElement('Button',['reload'],'Back to Menu');
                    reloadButton.addEventListener('click',function(){
                        util.sound.play('click');
                        location.reload();
                    });
                    game.board.showMessage('The game has ended',[reloadButton]);
                },util.wait*1.5);
            });
        }
    }
    
    class Player {
        constructor(name) {
            this.name = name;
            this.resources = {};
            this.resources[WONDER] = 0;
            this.resources[SHIELD] = 0;
            this.resources[SCIENCE] = 0;
            this.resources[COIN] = 3;
            this.resources[WORKER] = 0;
            this.resources[VP] = 0;
                        
            this.civs = [];   
            this.wonders = [];                     
            this.inventions = [];
            this.bonusses = [new Bonus(REST,COIN,2),new Bonus(REST,WORKER,1)];
            this.special = null; // special ability   
            this.tradeCount = 0;
            
            this.mainCiv;
            this.freeWorkers = 0;
            this.workersPlaced = 0;           
            this.availableInventions = new Set([7,8,9]);
            this.alwaysActions = [9,10,10,11];
            this.availableActions = [];
            
            this.bonusEventBacklog = [];
            this.currentBonusEvent;
            this.eventCallback;
        }
        
        checkBonusList(bonusList, event, cardClass,callback) {            
            if(callback) {
                this.eventCallback = callback;
            }
            
            for(let bonus of bonusList) {
                if(event === GAINED || event === RISE) {
                    this.bonusses.push(bonus);
                }
                if(cardClass) {
                    bonus.cardClass = cardClass;
                }
                if(bonus.check(event)) {
                    this.bonusEventBacklog.unshift(new BonusEvent(bonus,game.mode));
                }                
            }
            if(this.bonusEventBacklog.length === 0) {
                if(this.eventCallback) {                    
                    let myCallback = this.eventCallback;
                    this.eventCallback = null;
                    myCallback();                                        
                }
            }
            else {
                this.doBonusEventBackLog();
            }
            game.board.updateInfo();
        }       
        
        addResource(type,amount,cardClass) {
            let self = this;      
            if(cardClass) {
                setTimeout(function(){
                    self.resources[type] += Math.floor(amount);
                    if(type === WORKER && self.resources[WORKER] > 4) {
                        self.resources[WORKER] = 4;
                    }
                    if(self.resources[type] < 0) {
                        self.resources[type] = 0;
                    }
                    game.board.updateInfo();
                },util.wait);
                if(amount > 0) {
                    game.board.addIcon(type,q('.'+cardClass),q('.infoLabel .icon_'+type));
                }
            }
            else {
                this.resources[type] += Math.floor(amount);
                game.board.updateInfo();
            }
            if(type === COIN) {
                util.playMultiSound('coin',amount);
            }
            if(type === SHIELD && this.name !== '#Opponent') {
                util.playMultiSound('sword',amount);
            }
            if(type === VP) {                
                game.addHistory('VP',amount);
                util.sound.play('trumpet');
            }
        }
        
        removeMainCivBonusses() {
            this.special = null;  // wipe any possible special ability from old main civ
            let bonussesLeft = [];                        
            for(let bonus of this.bonusses) {
                if(!bonus.isMainCivBonus) {
                    bonussesLeft.push(bonus);
                }
                else {
                    bonus.revertBonus();         
                }
            }
            this.bonusses = bonussesLeft;
            
            if(this.mainCiv) {
                for(let actionId of this.mainCiv.containsMainAction) {                          
                    let indx = this.alwaysActions.indexOf(actionId);                    
                    if(indx !== -1) {
                        this.alwaysActions.splice(indx,1);
                    }
                }
            }
            for(let action of game.allActions) {
                let actionBonusLeft = [];
                for(let bonus of action.bonus) {
                    if(!bonus.isMainCivBonus) {
                        actionBonusLeft.push(bonus);
                    }
                    else {
                        bonus.revertBonus();
                    }                    
                }
                action.bonus = actionBonusLeft;
            }
        }
        
        resetActions() {
            this.availableActions = util.cloneObject(this.alwaysActions);   
            for(let node of qAll('#bottomActions .action')) {
                node.classList.add('disabled');
            }
            for(let r=0; r < game.actions.length; r++) {
                if(r >= game.round && (r < game.round+3 || this.special === 6)) { // special 6 : Maya may take future actions
                    this.availableActions.push(game.actions[r].id);
                    if(game.actions[r].id === 4) {
                        this.availableActions.push(4);  // add hunt twice
                    } 
                    q('.action_'+game.actions[r].id).classList.remove('disabled');                    
                }
            }      
        }
        
        doBonusEventBackLog() {
            if(this.bonusEventBacklog.length > 0) {
                this.currentBonusEvent = this.bonusEventBacklog[0];
                this.currentBonusEvent.do();
            }
        }
        
        researchPrices() {
            let specialPriceList = {};            
            let researchCost = game.getActionById(10).coinCost;
            let lowest = researchCost;
            for(let invent of this.availableInventions) {
                let thisInvention = game.getInventionById(invent);
                if(!this.inventions.find(i => i.id === invent)) {
                    specialPriceList[invent] = researchCost;
                    if(this.special === 1 && invent === 6) { //1 : Hittites can research Iron Working for 0 COIN
                        specialPriceList[invent] = 0;
                    }
                    
                    if(this.special === 2 && invent === 7) { // 2 : Sumer can research Writing and Sailing for 0 COIN
                        specialPriceList[invent] = 0;
                    }
                    if(this.special === 2 && invent === 8) { // 2 : Sumer can research Writing and Sailing for 0 COIN
                        specialPriceList[invent] = 0;
                    }

                    if(this.special === 3 && invent === 1) { // 3 : Ottoman can research Science and Gunpowder for 3 COINS less	
                        specialPriceList[invent] = Math.max(0,researchCost-3);
                    }
                    if(this.special === 3 && invent === 3) { // 3 : Ottoman can research Science and Gunpowder for 3 COINS less	
                        specialPriceList[invent] = Math.max(0,researchCost-3);
                    }
                    
                    
                    if(this.special === 7 && invent === 7) { // 7 : Harappa can research Writing, Sailing and Mining for 1 COINS less
                        specialPriceList[invent] = Math.max(0,researchCost-1);
                    }
                    if(this.special === 7 && invent === 8) { // 7 : Harappa can research Writing, Sailing and Mining for 1 COINS less
                        specialPriceList[invent] = Math.max(0,researchCost-1);
                    }
                    if(this.special === 7 && invent === 9) { // 7 : Harappa can research Writing, Sailing and Mining for 1 COINS less
                        specialPriceList[invent] = Math.max(0,researchCost-1);
                    }
                    if(specialPriceList[invent] < lowest) {
                        lowest = specialPriceList[invent];
                    }
                    
                    thisInvention.inventCost = specialPriceList[invent];
                }
            }
            specialPriceList[0] = lowest;
            return specialPriceList;
        }
    }
    
    class BonusEvent {
        constructor(bonus, oldMode) {
            this.id = util.getId();
            this.bonus = bonus;
            this.oldMode = oldMode;
        }
        
        do() {
            switch(this.bonus.what) {
                case WONDER:
                    game.changeMode(MODE_WONDER);
                    break;
                case SCIENCE:
                    if(game.player.availableInventions.size > 0) {
                        if(qId('leftBar').classList.contains('open')) {
                            setTimeout(function(){
                                qId('leftBar').classList.add('open');
                            },util.wait*3);                            
                        }
                        game.changeMode(MODE_SCIENCE);
                    }
                    else {
                        this.done();
                        game.player.eventCallback();
                    }
                    break;
                case FREEACTION:
                    game.changeMode(MODE_FREEACTION);
                    let self = this;
                    setTimeout(function(){
                        self.doFreeAction();
                    },util.wait*1.1);                  
                    break;  
                default:
                    if(this.bonus.cost) {
                        this.bonus.bonusWithCost();
                    }
                    else {
                        console.log('error: unknown bonusevent:',this.Bonus.what);
                    }
            }   
        }
        
        doFreeAction() {
            function freeActionCancel() {
                game.board.hideMessage();
                if(game.player.currentBonusEvent) {
                    game.player.currentBonusEvent.done();
                }
                game.player.checkBonusList([], null, null,null); // trigger callbacks stored on player.eventCallback
            }
            
            let self = this;
            let action = game.getActionById(this.bonus.amount);        
            let content = 'Do you wish to do a '+action.name+' action ?';
            let buttons = [];
            
            let freeActionCost = action.coinCost;
            if(action.id === 1 && game.player.civs.find(c => c.id === 35)) { // Harappa exception, counter bonus
                freeActionCost++;
            }
            if(freeActionCost < 0) {
                freeActionCost = 0;
            }
            if(freeActionCost <= game.player.resources[COIN]) {
                let buttonYes = document.createElement('button');   
                let buttonText = 'Yes, lose<br/>';
                if(freeActionCost === 0) {
                    buttonText = 'Yes<br/>';
                }
                
                for(let c =0; c < freeActionCost; c++) {
                    buttonText += '<div class="icon icon_coins"></div>';
                }                
                buttonYes.innerHTML = buttonText;
                buttonYes.addEventListener('click',function(){
                    util.sound.play('click');
                    game.board.hideMessage();            
                    if(game.player.currentBonusEvent) {
                        game.player.currentBonusEvent.done();
                    };
                    game.player.resources[COIN] -= freeActionCost;
                    game.board.updateInfo();
                    game.player.checkBonusList(action.bonus, INSTANT,'action_'+action.id, null);
                 });
                buttons.push(buttonYes);
            
                let buttonNo = document.createElement('button');   
                buttonNo.innerHTML = 'No';
                buttonNo.addEventListener('click',function(){  
                    util.sound.play('click');
                    freeActionCancel();
                });                        
                buttons.push(buttonNo);            

                game.board.showMessage(content,buttons);
            }
            else {
                freeActionCancel();
            }
        }
        
        done() {
            game.changeMode(this.oldMode);
            let index = game.player.bonusEventBacklog.findIndex(b => b.id === this.id);
            if(index !== -1) {
                game.player.bonusEventBacklog.splice(index,1);
            }            
        }
    }
    
    class Board {
        constructor() {
            this.createCivs();
            this.createWonders();
            this.createActions();
        }
        
        createCivs() {
            let civNodes = [];
            for(let civ of game.civs) {
                civNodes.push(civ.show());
            }
            qId('civs').append(...civNodes);
        }
        
        createWonders() {
            let wonderNodes = [];
            for(let wonder of game.wonders) {
                wonderNodes.push(wonder.show());
            }
            qId('wonderMarket').append(...wonderNodes);
        }
        
        createActions() {            
            let actionNodes = [];
            for(let action of game.actions) {
                actionNodes.push(action.show());                    
            }
            qId('bottomActions').append(...actionNodes);
        }
        
        updateInfo() {
            game.reCalculateDynamicBonusses();
            qId('vp').innerHTML = Math.max(0,game.player.resources[VP]);
            qId('coins').innerHTML = Math.max(0,game.player.resources[COIN]);
            qId('shield').innerHTML = Math.max(0,game.player.resources[SHIELD]);
            qId('worker').innerHTML = Math.min(4,game.player.freeWorkers) +'/'+ Math.min(4,game.player.resources[WORKER]);
            qId('wonder').innerHTML = Math.max(0,game.player.resources[WONDER]);
            qId('invention').innerHTML = Math.max(0,game.player.resources[SCIENCE]);
            qId('opponent').innerHTML = Math.max(0,game.opponent.resources[SHIELD]);
                        
            for(let action of game.allActions) {
                action.adjustWorkerClasses();
                action.adjustGains();
            }
            
            this.updateInventionCost();
        }
        
        updateCoinsOnCivs() {
            let coins = qAll('.card.civ .icon_coins');
            for(let coin of coins) {
                coin.remove();
            }
            for(let civ of game.civs) {
                if(civ.available) {
                    for(let c=0; c < civ.coins; c++) {
                        let coinDiv = document.createElement('div');
                        coinDiv.classList.add('icon');
                        coinDiv.classList.add('icon_coins');
                        coinDiv.classList.add('iconOnCiv');
                        q('.card.civ.civ_'+civ.id).append(coinDiv);
                    }
                }
            }
        }
        
        wipeFallenCiv(civ) {
            let workAreaNodes = qAll('.civ_'+civ.id+' .mainWorkerArea');
            for(let node of workAreaNodes) {
                node.remove();
            }
        }
        
        addWorkerToAction(action) {
            let query = '#playArea .workerArea_'+action+':not(.hasWorker)';
            let workAreaNodes = qAll(query);
            for(let node of workAreaNodes) {
                if(node.innerHTML === "") {
                    node.classList.add('hasWorker');
                    this.addIcon('worker',q('.infoLabel .icon_worker'),node,true);                    
                    break;
                }
            } 
        }
        
        removeAllWorkers() {
            let workAreaNodes = qAll('#playArea .workerArea .icon');
            for(let node of workAreaNodes) {
                node.parentElement.classList.remove('hasWorker');                
                node.style.opacity = 0.5;
                function myCallback() {
                    node.parentElement.innerHTML = '';
                }    
                if(!node.parentElement.classList.contains('sacrificeWorker')) {
                    this.moveElement(node,node.parentElement,qId('worker'),myCallback,util.randWait(),[node]);                               
                }
                else {
                    setTimeout(function(){
                        node.parentElement.innerHTML = '';
                    },util.wait);
                }
            }
        }
        
        takeCiv(civ, annex) {
            let selectedCiv = q('#civs .civ_'+civ.id);            
            let myCivs = q('#myCivContainer');
            
            function myCallback(myCivs,selectedCiv,annex) {
                if(annex) {
                    myCivs.append(selectedCiv);
                }
                else {
                    myCivs.prepend(selectedCiv);
                }
                selectedCiv.style.top = '';
                selectedCiv.style.left = '';
            }
            
            this.moveElement(selectedCiv, selectedCiv, myCivs, myCallback , util.wait,[myCivs,selectedCiv,annex],true);
            
        }
        
        addIcon(iconType, from, to, keep) {
            var newIconDiv = document.createElement('div'); 
            newIconDiv.classList.add('icon','icon_'+iconType);
            from.append(newIconDiv);
            function myCallback(newIconDiv, node, keep) {
                if(keep) {
                    to.append(newIconDiv);
                    newIconDiv.style.top = '';
                    newIconDiv.style.left = '';
                }
                else {
                    newIconDiv.remove();
                }
            }
            this.moveElement(newIconDiv, from, to, myCallback, util.randWait(), [newIconDiv, to, keep], keep);            
        }
        
        moveElement(element, fromElement, toElement, callback, delay, parameters, keep) {            
            let fromRect = fromElement.getBoundingClientRect();
            let toRect = toElement.getBoundingClientRect();
            let elementRect = element.getBoundingClientRect();
            let leftMovement = toRect.left - fromRect.left + Math.floor(toRect.width/2) - Math.floor(elementRect.width/2);
            let topMovement = toRect.top - fromRect.top + Math.floor(toRect.height/5);
            element.style.top = topMovement+'px';
            element.style.left = leftMovement+'px';
            element.style.transitionDuration = delay+'ms';
            if(!keep) {
                element.style.opacity = 0.5;
            }
            if(callback) {
                setTimeout(function(){
                    callback(...parameters);
                },delay);
            }
        }
        
        showMessage(content,buttons) {
            let messageBox = qId('messageBox');
            let messageText = qId('messageText');
            let messageButtons = qId('messageButtons');
            
            messageText.innerHTML = content;
            messageButtons.innerHTML = '';
            messageButtons.append(...buttons);
            messageBox.style.zIndex = 9;
            messageBox.style.opacity = 1;
            
            clearTimeout(this.messageTimeout);
        }
        
        hideMessage() {
            let messageBox = qId('messageBox');
            let messageText = qId('messageText');
            let messageButtons = qId('messageButtons');
            
            messageBox.style.zIndex = -1;
            messageBox.style.opacity = 0;
            
            
            this.messageTimeout = setTimeout(function(){
                messageText.innerHTML = '';
                messageButtons.innerHTML = '';
            },util.wait/2);
        }
        
        inventionReachable() {
            for(let techId of game.player.availableInventions) {
                q('.tech.science_'+techId).classList.add('reachable');
            }
        }
        
        toast(text, type,topIndex) {
            let newToast = document.createElement('div');
            newToast.classList.add('toast');
            newToast.classList.add('toast_'+type);
            newToast.innerHTML = text;
            newToast.addEventListener('click',function() {
                util.sound.play('click');
                newToast.style.opacity = 0;
                setTimeout(function(){
                    newToast.remove();
                },util.wait/4);
            });
            if(topIndex) {
                newToast.style.zIndex = 1000;
            }
            qId('toastContainer').prepend(newToast);
            if(type !== MODE_GAMEOVER && type !== ACHIEVEMENT) {
                setTimeout(function() {
                    newToast.style.opacity = 0;
                    setTimeout(function(){
                        newToast.remove();
                    },util.wait/4);
                },util.wait*4); 
            }
        }
        
        updateInventionCost() {
            let researchAction = game.getActionById(10);
            for(let tech of qAll('.tech.scienceExtraCost')) {
                tech.classList.remove('scienceExtraCost','scienceCost_0', 'scienceCost_1', 'scienceCost_2', 'scienceCost_3', 'scienceCost_4');
            }
            if(researchAction.coinCost !==  researchAction.mixedLowCost){
                let priceList = game.player.researchPrices();
                let alreadyPaid = researchAction.mixedLowCost;
                for(let invent of game.player.availableInventions) {
                    let thisInvention = game.getInventionById(invent);
                    if(!game.player.inventions.find(i => i.id === invent)) {
                        let thisPrice = thisInvention.inventCost;
                        thisPrice = thisPrice - alreadyPaid; // already paid
                        if(thisPrice <= 0) {
                            thisPrice = 0;
                        }
                        else {
                            q('.tech.science_'+invent).classList.add('scienceCost_'+thisPrice, 'scienceExtraCost');
                        }
                    }
                }   
            }
        }
        
        removeMainCivWorkerAreas() {
            if(game.player.mainCiv) {
                this.removeCivWorkerAreas(game.player.mainCiv.id);
            }
        }
        
        removeCivWorkerAreas(civId) {
            for(let area of qAll('.civ_'+civId+' .mainWorkerArea')) {
                    area.remove();
                }
        }
    }
    
    class Achievement {
        constructor(id,name,reqText,requirements) {
            this.id = id;
            this.name = name;
            this.reqText = reqText;
            this.requirements = requirements;
            this.achieved = false;            
            this.when = this.whenAchieved();            
            if(this.when) {
                this.achieved = true;
            }
            util.achievements.push(this);
        }
        
        whenAchieved() {
            return util.playerAchievements[this.id-1];
        }
    }
    
    class Civilisation {
        constructor(id,name,workers,mainBonus, legacyBonus, unofficial) {
            this.id = id;
            this.name = name;            
            this.coins = 0;
            this.workers = parseInt(workers);
            this.mainBonus = mainBonus;
            this.legacyBonus = legacyBonus;      
            this.containsMainAction = [];
            this.containsLegacyAction = [];
            this.annexed = false;
            this.available = true;
            if(unofficial) {
                this.unofficial = true;
            }
            else {
                this.unofficial = false;
            }            
            
            for(let bonus of this.mainBonus) {
                bonus.isMainCivBonus = true;
                if(bonus.what === NEWACTION) {
                    this.containsMainAction.push(bonus.amount);
                }
            }
            
            for(let bonus of this.legacyBonus) {
                if(bonus.what === NEWACTION) {
                    this.containsLegacyAction.push(bonus.amount);
                }
            }
            
            game.allCivs.push(this);
            game.civDeck.push(this);
        }
        
        show(readOnly) {
            let self = this;
            var newCivDiv = document.createElement('div'); 
            newCivDiv.classList.add('card','civ','civ_'+this.id);
            if(this.containsMainAction.length === 2) {
                newCivDiv.classList.add('workareas_2'); 
            }
            newCivDiv.style.backgroundImage = "url('img/civ"+this.id+".jpg?v1_1')";
            for(let area = 0; area < this.containsMainAction.length; area++) {
                let newAreaDiv = document.createElement('div'); 
                newAreaDiv.classList.add('workerArea', 'workerArea_'+this.containsMainAction[area], 'mainWorkerArea', 'mainWorkerArea_'+this.containsMainAction[area],'workerAreaLoc_'+area);
                if((this.id === 2 && this.containsMainAction[area] === 1) ||
                   (this.id === 40 && this.containsMainAction[area] === 2) ||
                   (this.id === 22 && this.containsMainAction[area] === 2)
                ) {
                    newAreaDiv.classList.add('changedWorkerArea');
                }
                newCivDiv.append(newAreaDiv);
            }
            for(let area = 0; area < this.containsLegacyAction.length; area++) {
                let newAreaDiv = document.createElement('div'); 
                newAreaDiv.classList.add('workerArea', 'workerArea_'+this.containsLegacyAction[area], 'legacyWorkerArea', 'legacyWorkerArea_'+this.containsLegacyAction[area],'workerAreaLoc_'+area);
                if((this.id === 40 && this.containsLegacyAction[area] === 8) ||
                   (this.id === 37 && this.containsLegacyAction[area] === 2) ||
                   (this.id === 53 && this.containsLegacyAction[area] === 2) ||
                   (this.id === 11 && this.containsLegacyAction[area] === 8) ||
                   (this.id === 27 && this.containsLegacyAction[area] === 2) ||
                   (this.id === 51 && this.containsLegacyAction[area] === 2) ||
                   (this.id === 56 && this.containsLegacyAction[area] === 9)
                ) {
                    newAreaDiv.classList.add('changedWorkerArea');
                }
                newCivDiv.append(newAreaDiv);
            }
            
            if(!readOnly) {
                newCivDiv.addEventListener('click',function(){
                    self.clicked();
                });
                
                newCivDiv.addEventListener('mousedown',function(){
                clearTimeout(util.holdTimeout);
                util.holdTimeout = setTimeout(function(){
                   newCivDiv.classList.add('enlarge');
                },util.wait);
            });
            }
            
            return newCivDiv;
        }
        
        addCoin(coins) {
            this.coins += coins;
        }
        
        clicked() {
            if(game.player.civs.length < 3 && !game.player.civs.find(c => c.id === this.id)) {
                if(game.mode === MODE_RISE) {
                    q('.civ_'+this.id).removeEventListener('click',this.clicked);
                    util.sound.play('click');
                    this.take(false);
                }
                if(game.mode === MODE_ANNEX) {
                    q('.civ_'+this.id).removeEventListener('click',this.clicked);
                    util.sound.play('click');
                    this.annexed = true;
                    this.take(true);
                }
            }
        }
        
        take() {
            if(!this.annexed) {
                game.addHistory('Rise',this.id);
            }
            else {
                game.addHistory('Annex',this.id);
            }
            let self = this;
            game.changeMode(MODE_WAIT);
            game.board.takeCiv(this,this.annexed);
            game.player.civs.push(this);
            let newWorkers = 0;
            if(!this.annexed) {                
                game.player.removeMainCivBonusses();
                game.board.removeMainCivWorkerAreas();                
                game.player.mainCiv = this;
                newWorkers = this.workers;
            }
            else {
                game.board.removeCivWorkerAreas(this.id);
                if(game.player.resources[WORKER] < 4) {
                    newWorkers = 1;
                }
            }
            self.available = false;
            setTimeout(function(){                
                self.getWorkers(!self.annexed,newWorkers);                             
                self.getBonusses();
                if(game.player.special === 10) {
                    game.player.addResource(VP, 2,'civ_'+game.player.mainCiv.id);
                }
                if(self.coins > 0) {
                    let coins = qAll('.civ_'+self.id+' .icon_coins');
                    for(let coin of coins) {
                        coin.remove();
                    }
                    game.player.addResource(COIN,self.coins,'civ_'+self.id);
                    self.coins = 0;
                }
                else {
                    game.board.updateInfo();
                }
            },util.wait);            
            this.addWorkerAreaClicks();
            game.player.resetActions();
        }
        
        getWorkers(reset,newWorkers) {
            if(reset) {
                game.player.resources[WORKER] = 0;
                game.player.freeWorkers = 0;
                game.player.workersPlaced = 0;
            }
            else {
                game.player.freeWorkers = game.player.resources[WORKER];
            }
            if(newWorkers) {
                for(let w=0; w < newWorkers; w++) {
                    game.player.addResource(WORKER,1,'civ_'+this.id);
                    game.player.freeWorkers++;
                    game.player.workersPlaced++;
                    if(game.player.freeWorkers > 4) {
                        game.player.freeWorker = 4;
                    }
                    if(game.player.workersPlaced > 4) {
                        game.player.workersPlaced = 4;
                    }
                }   
            }
            else {
                game.board.updateInfo();
            }
        }
        
        getBonusses() {
            let self = this;
            game.player.checkBonusList(this.legacyBonus, this.annexed?GAINED:RISE, 'civ_'+this.id,function(){
                if(!self.annexed) {
                    game.player.checkBonusList(self.mainBonus, RISE, 'civ_'+self.id,function(){
                        if(game.mode === MODE_WAIT) {
                            setTimeout(function(){
                                game.phase2();
                            },util.wait);
                        }
                    });
                }
                else {
                    if(game.mode === MODE_WAIT) {
                        setTimeout(function(){
                            game.phase2();
                        },util.wait);
                    }
                }
            });            
        }
        
        addWorkerAreaClicks() {
            for(let actionId of this.containsLegacyAction) {
                let action = game.getActionById(actionId);
                if(action) {
                    this.addWorkerAreaClick(action, false);
                }
            }
            for(let actionId of this.containsMainAction) {
                let action = game.getActionById(actionId);
                if(action) {
                    this.addWorkerAreaClick(action, true);
                }
            }
        }
        
        addWorkerAreaClick(action, isMainAction) {
            let areaType = 'legacyWorkerArea';
            if(isMainAction)  {
                areaType = 'mainWorkerArea';
            }
            let workerAreaNode = q('.civ_'+this.id+' .'+areaType+'_'+action.id);
            if(workerAreaNode) {
                workerAreaNode.addEventListener('click',function(){
                    if(game.mode === MODE_WORKERS) {
                        util.sound.play('click');
                        action.use();
                    }
                });
               workerAreaNode.classList.add('hasActionEvent');
            }            
        }
    }
    
    class Wonder {
        constructor(id,name,bonus) {
            this.id = id;
            this.name = name;
            this.bonus = bonus;
            
            game.allWonders.push(this);
        }
        
        show() {
            let self = this;
            var newWonderDiv = document.createElement('div'); 
            newWonderDiv.classList.add('card','wonder','wonder_'+this.id);
            newWonderDiv.style.setProperty('--wonderNum', this.id -1);             
            newWonderDiv.addEventListener('click',function(){
                self.clicked();
            });
            
            newWonderDiv.addEventListener('mousedown',function(){
                clearTimeout(util.holdTimeout);
                util.holdTimeout = setTimeout(function(){
                   newWonderDiv.classList.add('enlarge');
                },util.wait);
            });
            
            return newWonderDiv;
        }
        
        clicked() {
            if(game.mode === MODE_WONDER) {      
                util.sound.play('click');
                this.addWonder();
            }
        }
        
        addWonder() {
            game.addHistory('Wonder',this.id);
            game.player.wonders.push(this);
            game.player.addResource(WONDER,1);
            qId('myWonders').append(q('#wonderMarket .wonder_'+this.id));
            if(game.player.currentBonusEvent) {
                game.player.currentBonusEvent.done();
            }
            game.player.checkBonusList(this.bonus, GAINED, null, null);
            qId('wonders').classList.add('hasWonders');
        }
    }
    
    class Action {
        constructor(id, name, coinCost, workerCost, sacrifice, bonus, war) {
            this.id = id;
            this.name = name;
            this.coinCost = coinCost;
            this.baseCoinCost = coinCost;
            this.mixedLowCost = coinCost;
            this.workerCost = workerCost;
            this.sacrifice =  sacrifice;
            this.bonus = bonus;
            this.war = war;
            this.workerAreas = 1;
            
            if(this.id === 4 || this.id === 10) {
                this.workerAreas = 2;
            }
            
            game.allActions.push(this);
        }
        
        use() {            
            let self = this;
            if(game.player.availableActions.indexOf(this.id) === -1) {
                game.board.toast('Action not available',ERROR);
                return;
            }
            if(this.workerCost > game.player.freeWorkers) {
                game.board.toast('Not enough workers',ERROR);
                return;
            }            
            if(this.coinCost > game.player.resources[COIN] && !(this.id === 10 && this.mixedLowCost <= game.player.resources[COIN])) {
                game.board.toast('Not enough coins',ERROR);
                return;
            }     
                        
            game.addHistory('Action',this.id);
            if(this.id === 10 && this.mixedLowCost <= game.player.resources[COIN]) {
                game.player.resources[COIN] -= this.mixedLowCost;
            }
            else {
                game.player.resources[COIN] -= this.coinCost;
            }
            game.player.freeWorkers -= this.workerCost;
            if(!this.sacrifice) {
                game.player.workersPlaced += this.workerCost;
            }
            else {
                if(game.player.special === 12) {
                    game.player.addResource(COIN, 1,'civ_58');
                }
            }
            let actionIndex = game.player.availableActions.indexOf(this.id);
            game.player.availableActions.splice(actionIndex,1);
            game.board.updateInfo();
            game.board.addWorkerToAction(this.id);
            
            setTimeout(function(){         
                if(game.player.special === 11 && self.id === 7) { // SPECIAL 11 : Portugal, Trade twice in a turn to gain 1 worker
                    game.player.tradeCount++;     
                    if(game.player.tradeCount === 2) {
                        if(game.player.resources[WORKER] < 4) {
                            game.player.freeWorkers++;
                        } 
                        game.player.addResource(WORKER,1,'civ_55');                        
                    }
                } 
                game.player.checkBonusList(self.bonus, INSTANT,'action_'+self.id, function() {
                    if(self.id === 2 && game.player.special === 8) { // 9 : Aksum when you build, you may do culture without workers
                        let specialBonusEvent = new BonusEvent(new Bonus(INSTANT,FREEACTION,1),game.mode);
                        game.player.bonusEventBacklog.unshift(specialBonusEvent);
                        game.player.checkBonusList([], null, null,function(){
                            if(game.player.freeWorkers <= 0) {
                                game.endRound();
                            }
                        }); 
                    }
                    else {
                        if(game.player.freeWorkers <= 0) {
                            game.endRound();
                        }
                    }
                });                
            },util.wait);            
        }
        
        show() {
            let self = this;
            var newActionDiv = document.createElement('div'); 
            newActionDiv.classList.add('card','action','action_'+this.id);
            newActionDiv.style.setProperty('--actionNum', this.id -1); 
            let workerBlockDiv = document.createElement('div'); 
            workerBlockDiv.classList.add('workerBlock');
            for(let area = 0; area < this.workerAreas; area++) {
                let newAreaDiv = document.createElement('div'); 
                newAreaDiv.classList.add(...this.workerAreaClass());                
                workerBlockDiv.append(newAreaDiv);
            }
            let newGainDiv1 = document.createElement('div'); 
            newGainDiv1.classList.add('actionGain1');
            let newGainDiv2 = document.createElement('div'); 
            newGainDiv2.classList.add('actionGain2');
            newActionDiv.append(workerBlockDiv);
            newActionDiv.append(newGainDiv1);
            newActionDiv.append(newGainDiv2);
            
            if(this.id !== 5) {
                newActionDiv.addEventListener('click',function(){
                    self.clicked();
                });
            }
            
            return newActionDiv;
        }
        
        workerAreaClass(extraClass) {
            let classes = ['changedWorkerArea','workerArea','workerArea_'+this.id];
            if(this.sacrifice) {
                if(this.workerCost === 2) {
                    classes.push('sacrificDoubleWorker');
                }
                else {
                    classes.push('sacrificeWorker');
                }                
            }     
            else {
                if(this.workerCost === 2) {
                    classes.push('doubleWorker');
                }
                else {
                    classes.push('singleWorker');
                }
            }
            if(this.coinCost > 0) {
                if(this.id === 10) {
                    this.mixedLowCost = game.player.researchPrices()[0];
                    classes.push('workerCost_'+this.mixedLowCost);
                }
                else {
                    classes.push('workerCost_'+this.coinCost);
                }
            }
            if(this.id === 10 && game.player.availableInventions.size === 0) {
                classes.push('hidden');
            }
            return classes;
        }
        
        adjustWorkerClasses() {
            let allWorkerAreas = qAll('.action .workerArea_'+this.id);
            let newClasses = this.workerAreaClass();
            for(let area of allWorkerAreas) {
                let hasWorker = false;
                if(area.classList.contains('hasWorker')) {
                    hasWorker = true;
                }
                area.className = '';
                area.classList.add(...newClasses);
                if(hasWorker) {
                   area.classList.add('hasWorker'); 
                }
            }
            
            let allCivWorkerAreas = qAll('.civ .changedWorkerArea.workerArea_'+this.id);
            for(let area of allCivWorkerAreas) {
                area.classList.remove('sacrificDoubleWorker','sacrificeWorker','doubleWorker','singleWorker','workerCost_0','workerCost_1','workerCost_2');
                area.classList.add(...newClasses);
            }
                        
        }
        
        adjustGains() {            
            let gainHTML = '';
            let gains = {};
            let gain2HTML = '';
            let gains2 = {};
            for(let bonus of this.bonus) {
                if(bonus.when === INSTANT) {
                    let skip = false;
                    if(bonus.condition) {
                        if(this.id === 4 && bonus.condition === 1) {
                            if(gains[bonus.what]) {
                                gains2[bonus.what] += bonus.amount;
                            }
                            else {
                                gains2[bonus.what] = bonus.amount;
                            }
                            skip = true;
                        }
                        else {
                            if(this.id === 3 && bonus.condition === 4) {}
                            else {
                                if(!game.checkSpecialCondition(bonus.condition)) {
                                    skip = true;
                                }
                            }
                        }
                    }
                    if(!skip) {
                        if(gains[bonus.what]) {
                            gains[bonus.what] += bonus.amount;
                        }
                        else {
                            gains[bonus.what] = bonus.amount;
                        }
                    }
                }
            }
            gains[WONDER] = 0;
            gains[SCIENCE] = 0;
            
            for(let gain in gains) {
                if(gains.hasOwnProperty(gain)) {
                    for(let g=0; g < gains[gain]; g++) {
                        gainHTML += '<div class="icon icon_'+gain+'"></div>';
                    }
                }
            }

            // TODO: ugly copy of code, make into single called function

            if(gainHTML !== '') {
                gainHTML = '+'+gainHTML;
            }
            let allActionGains = qAll('.action_'+this.id+' .actionGain1');
            for(let gainDiv of allActionGains) {
                 gainDiv.innerHTML = gainHTML;
            }
            
            for(let gain2 in gains2) {
                if(gains2.hasOwnProperty(gain2)) {
                    for(let g=0; g < gains2[gain2]; g++) {
                        gain2HTML += '<div class="icon icon_'+gain2+'"></div>';
                    }
                }
            }
            
            if(gain2HTML !== '') {
                gain2HTML = '+'+gain2HTML;
            }
            let allActionGains2 = qAll('.action_'+this.id+' .actionGain2');
            for(let gainDiv2 of allActionGains2) {
                 gainDiv2.innerHTML = gain2HTML;
            }
            
            //
        }
        
        addClickEvent() {
            let self = this;
            q('.bigAction .action_'+this.id).addEventListener('click',function(){
                self.clicked();
            });
        }
        
        clicked() {            
            if(game.mode === MODE_WORKERS) {
                util.sound.play('click');
                if(this.id === 10 && game.player.availableInventions.size === 0) {
                    // nothing to invent, ignore click.
                }
                else {
                    this.use();
                }
            }
        }
    }
    
    class Invention {
        constructor(id,name,bonus,leadsTo) {
            this.id = id;
            this.name = name;
            this.bonus = bonus;
            this.leadsTo = leadsTo;
            this.inventCost = 0;
            
            game.inventions.push(this);
        }
        
        learn() {
            if(game.player.currentBonusEvent) {
                game.player.currentBonusEvent.done();
            }
            else {
                console.log('error: no return mode !');
                game.changeMode(MODE_WORKER);
            }
            
            game.addHistory('Tech',this.id);
            q('.science_'+this.id).classList.add('known');
            game.player.inventions.push(this);
            for(let inventionId of this.leadsTo) {
                if(!game.player.inventions.find(i => i.id === inventionId)) {
                    game.player.availableInventions.add(inventionId);
                }
            }
            game.player.availableInventions.delete(this.id);
            
            game.board.inventionReachable();
            game.player.checkBonusList(this.bonus, GAINED,'science_'+this.id,null);
            if(game.player.special === 5) {  //special 5 : Korea gains 2 VP when researching Writing, Education or Sailing
                if(this.id === 4 || this.id === 7 || this.id === 8) {
                    game.player.addResource(VP,2,'civ_24');
                }
            }
            game.player.addResource(SCIENCE,1,'science_'+this.id);
            setTimeout(function(){
                qId('leftBar').classList.remove('open');
            },util.wait*2);
        }
        
        clicked() {
            if(game.mode === MODE_SCIENCE) {
                if(!game.player.inventions.find(i => i.id === this.id) && game.player.availableInventions.has(this.id)) {
                    util.sound.play('click');
                    let researchAction = game.getActionById(10);
                    let alreadyPaid = researchAction.coinCost;
                    if(researchAction.coinCost !==  researchAction.mixedLowCost){
                        alreadyPaid = researchAction.mixedLowCost;
                    }
                    let learnCost = this.inventCost-alreadyPaid;
                    if(learnCost > game.player.resources[COIN]) {
                        game.board.toast('Not enough coins',ERROR,true);
                        return;
                    }  
                    if(learnCost > 0) {
                        game.player.resources[COIN] -= learnCost;
                    }
                    this.learn();
                }
            }
        }
    }
    
    class Bonus {
        constructor(when,what,amount,onAction,per,cost,costAmount,condition,ofOpponent, dynamic) {
            this.id = util.getId();
            this.when = when;                   //END, INSTANT, WAR, CHANGE 
            this.what = what;
            this.amount = amount;
            this.onAction = onAction;
            this.per = per;
            this.cost = cost;
            this.costAmount = costAmount;
            this.condition = condition; 
            this.ofOpponent = ofOpponent;
            this.isMainCivBonus = false;
            this.cardClass = '';
            
            if(dynamic) {
                this.dynamic = true;
                this.dynamicOnAction = onAction;
                this.dynamicAmount = amount;
                this.dynamicBonusValue = 0;
            }            
            
            game.allBonusses.push(this);
        }
        
        triggerBonus(ignoreCost) {
            if(this.cost && !ignoreCost) {
                if(game.player.resources[this.cost] >= this.costAmount) {                  
                    return true;                                        
                }
            }
            else {
                
                if(this.what === SPECIAL) {
                    this.doSpecial();
                    return false;
                }
                if(this.what === NEWACTION) {
                    let newAction = this.amount; //amount contains the new actions here !
                    game.player.alwaysActions.push(newAction);
                    return false;
                }
                if(this.what === FREEACTION) {
                    return true;
                }
                
                if(this.what === WONDER) {
                    return true;
                }
                
                if(this.what === SCIENCE) {
                    return true;
                }

                if(this.per) {
                    let checkResources = game.player.resources;
                    if(this.ofOpponent) {
                        checkResources = game.opponent.resources;
                    }
                    if(this.onAction) {
                        let action = game.getActionById(this.onAction);
                        if(this.what === COINCOST) {
                            action.coinCost += Math.floor(checkResources[this.per] * this.amount);
                            action.coinCost = Math.max(0,action.coinCost);
                        }
                    }
                    else {
                        if(this.per === FUTURE) { 
                            game.player.addResource(this.what,(5-game.round) * this.amount,this.cardClass); // gain per future round
                        }
                        else {
                            game.player.addResource(this.what,checkResources[this.per] * this.amount,this.cardClass);
                        }
                    }
                    return false;
                }

                if(this.onAction) {
                    let action = game.getActionById(this.onAction);
                    if(action) {
                        if(this.what === COINCOST) {
                            action.coinCost += this.amount;
                            action.coinCost = Math.max(0,action.coinCost);                            
                        }
                        else {
                            if(this.what === WORKERCOST) {
                                action.workerCost += this.amount;
                                action.workerCost = Math.max(0,action.workerCost);
                            }  
                            else {
                                if(this.what === SACRIFICE) {
                                    if(this.amount <= 0) {
                                        action.sacrifice = false;
                                    }
                                    else {
                                        action.sacrifice = true;
                                    }
                                }  
                                else {
                                    this.onAction = null;
                                    this.when = INSTANT;
                                    action.bonus.push(this);
                                }
                            }
                        }                        
                        return false;
                    }
                }
                
                game.player.addResource(this.what,this.amount,this.cardClass);
                if(this.what === WORKER && this.amount > 0 && game.player.resources[WORKER] < 4) {  // China can add workers that instantly becomes active if you had less than 4 workers
                    game.player.freeWorkers = Math.min(game.player.freeWorkers + this.amount, 4);
                }
            }
            return false;
        }
        
        bonusWithCost() {
            function bonusCostDone() {
                game.board.hideMessage();
                if(game.player.currentBonusEvent) {
                    game.player.currentBonusEvent.done();
                }
                game.player.checkBonusList([], null, null,null); // trigger callbacks stored on player.eventCallback
            }                    
                    
            let self = this;    
            let content = 'Do you wish to buy ';
            let buttons = [];
            for(let c = 0; c < this.amount; c++) {
                content += '<div class="icon icon_'+this.what+'"></div>';
            }            
            content += ' ?';
            
            if(this.costAmount <= game.player.resources[this.cost]) {
                let buttonYes = document.createElement('button');   
                let buttonText = 'Yes, lose<br/>';
                for(let c =0; c < this.costAmount; c++) {
                    buttonText += '<div class="icon icon_'+this.cost+'"></div>';
                }                
                buttonYes.innerHTML = buttonText;
                buttonYes.addEventListener('click',function(){
                    game.board.hideMessage();            
                    util.sound.play('click');
                    if(game.player.currentBonusEvent) {
                        game.player.currentBonusEvent.done();
                    };
                    game.player.resources[self.cost] -= self.costAmount;
                    self.triggerBonus(true);
                    bonusCostDone();
                 });
                buttons.push(buttonYes);
            
                let buttonNo = document.createElement('button');   
                buttonNo.innerHTML = 'No';
                buttonNo.addEventListener('click',function(){     
                        util.sound.play('click');
                    bonusCostDone();
                });                        
                buttons.push(buttonNo);            

                game.board.showMessage(content,buttons);
            }
            else {
                freeActionCancel();
            }
        }
                        
        check(trigger) {
            if(this.dynamic) {
                game.dynamicBonusses.add(this);
                return;
            }
            if(this.condition) {
                if(!game.checkSpecialCondition(this.condition)) {
                    return;
                }
            }
            if(trigger === GAINED || trigger === RISE || trigger === ANNEX) {
                if(this.when === INSTANT) {
                    return this.triggerBonus();
                }
                if(this.when === CHANGE) {
                    return this.triggerBonus();
                }
            }
            if(trigger  === this.when) {
                return this.triggerBonus();
            }          
        }
        
        revertBonus() {
            if(this.onAction) {
                let action = game.getActionById(this.onAction);
                if(action) {
                    if(this.what === COINCOST && this.amount < 0) {
                        action.coinCost -= this.amount;
                        action.coinCost = Math.max(0,action.coinCost);
                    }
                    else {
                        if(this.what === WORKERCOST) {
                            action.workerCost -= this.amount;
                            action.workerCost = Math.max(0,action.workerCost);
                        }
                        else {
                            if(this.what === SACRIFICE) {
                                if(this.amount <= 0) {
                                        action.sacrifice = true;
                                    }
                                    else {
                                        action.sacrifice = false;
                                    }
                            }
                            else {
                                action.bonus.push(new Bonus(INSTANT, this.what,-this.amount));
                            }
                        }
                    }  
                }
            }
            else {
                if(this.dynamic) {
                    game.player.resources[this.what] -= this.amount;
                } 
            }
            if(this.what === NEWACTION) {
                let newAction = this.amount; //amount contains the new action here !
                let actionIndex = game.player.alwaysActions.indexOf(newAction);
                if(actionIndex !== -1) {
                    game.player.alwaysActions.splice(actionIndex,1);  // remove the action from the list of always available actions
                }     
            }         
            
            if(this.when === INSTANT && this.what === SHIELD) {
                game.player.resources[this.what] -= this.amount;
            }
        }
        
        doSpecial() {
            if(this.amount === 13)  {
                if(game.player.resources[WORKER] === 0) {
                    game.player.addResource(VP, 3,'civ_58');
                }
            }
            if(this.amount === 9)  {
                // do Benin special:               
                let vpGained = Math.floor(game.player.resources[COIN]/3);
                game.player.resources[COIN] -= vpGained * 3;
                game.player.addResource(VP, vpGained,'civ_53');
            }
            game.player.special = this.amount; // give player a special bonus not catched normally (used to identify active specials)            
        }
    }

    class Menu {
        constructor() {
            this.highScores;
            this.weekHigh;
            this.monthHigh;
            this.civScores;
            this.myBestScores;
            this.additionalCivs = [];
            this.sortBy = 0;
            this.scoreType = 0;
        }
        
        init() {
            if(util.useLocalStorage('aocMoreCivs',false,true)) {
                qId('unofficial').checked = true;
            }
            this.toggleAdditionalCivs();
            qId('landing').classList.add('fadeIn');     
            qId('startGame').addEventListener('click',function(){
                util.sound.play('click');
                qId('landing').classList.remove('fadeIn');                     
                game.start();
            }); 
            qId('showAchievements').addEventListener('click',function(){
                util.sound.play('click');
                qId('achievementModal').classList.add('visible');     
            }); 
            qId('showHelp').addEventListener('click',function(){
                util.sound.play('click');
                new Help();
            }); 
            qAll('.closeModal').forEach(closeButton => closeButton.addEventListener('click',function() {
                util.sound.play('click');
                qId('achievementModal').classList.remove('visible'); 
                qId('help').classList.remove('visible'); 
            }));
            qId('unofficialCivs').addEventListener('click',function() {
                menu.toggleAdditionalCivs();
            });
            document.addEventListener( "click", function(event){                    
                var element = event.target;
                if(element.classList.contains("sortCivArrow")) {
                    menu.civScoreOrder();
                }
            });
            document.addEventListener( "click", function(event){                    
                var element = event.target;
                if(element.classList.contains("typeCivArrow")) {
                    menu.civScoreType();
                }
            });
            
            let addedCivs = game.allCivs.filter(c => c.unofficial === true);
            for(let addCiv of addedCivs) {
                this.additionalCivs.push(addCiv.id);
            }
            
            this.initScores();  
            this.showAchievements();
        }
        
        initScores(selfOnly) {
            if(!selfOnly) {
                util.xhr('//boardwebgames.com/aoc/server.php?monthhighscores',null,function(data){
                    menu.monthHigh = JSON.parse(data);  
                    menu.showHighScores(null,qId('highScores'));
                    menu.showHighScores(30,qId('shortHighScores')); 

                });
                util.xhr('//boardwebgames.com/aoc/server.php?weekhighscores',null,function(data){
                    menu.weekHigh = JSON.parse(data);    
                });
                util.xhr('//boardwebgames.com/aoc/server.php?highscores',null,function(data){
                    menu.highScores = JSON.parse(data);
                });
                util.xhr('//boardwebgames.com/aoc/server.php?highcivs',null,function(data){
                    menu.civScores = JSON.parse(data);
                    menu.showCivScores(null,qId('civScores'));
                    menu.showCivScores(20,qId('shortCivScores'));               
                });
                
                document.addEventListener( "click", function(event){                    
                    var element = event.target;
                    if(element.classList.contains("scoreTitle")) {
                        if(element && element.parentElement && element.parentElement.parentElement) {
                            let largeList = element.parentElement.parentElement.querySelector('.largeScoreList');
                            if(largeList) {
                                util.sound.play('click');
                                largeList.classList.toggle('visible');
                            }
                        }
                    }
                });                                
            }
            
            util.xhr('//boardwebgames.com/aoc/server.php?mybest='+encodeURI(playerName),null,function(data){
                menu.myBestScores = JSON.parse(data);
                menu.showPlayerScores();                
            });          
        }
        
        civScoreType() {
            this.scoreType++;
            if(this.scoreType > 2) {
                this.scoreType = 0;
            }
            menu.showHighScores(null,qId('highScores'));
            menu.showHighScores(30,qId('shortHighScores')); 
        }
        
        civScoreOrder() {
            this.sortBy++;
            if(this.sortBy > 2) {
                this.sortBy = 0;
            }
            menu.showCivScores(null,qId('civScores'));
            menu.showCivScores(30,qId('shortCivScores')); 
        }
        
        showCivScores(size, target) {
            if(!size) {
                size = this.civScores.length +1;
            }
            let civScoreElement = document.createElement('div');
            civScoreElement.classList.add('scoreWrapper');        
            for(let civScore of this.civScores) {
                let score = parseInt(civScore.score);      
                let played = parseInt(civScore.count);
                civScore.average = Math.round((score/played)*10)/10;
            }
            if(this.sortBy === 0) {
                this.civScores.sort((a,b) => b.average - a.average);
            }
            if(this.sortBy === 1) {
                this.civScores.sort((a,b) => b.score - a.score);
            }
            if(this.sortBy === 2) {
                this.civScores.sort((a,b) => b.count - a.count);
            }
            for(let [index,civScore] of this.civScores.entries()) {
                if(index >= size) {
                    break;
                }                
                let civId = civScore.civ;
                if(civId) {
                    let civ = game.getCivById(civId);
                    let score = civScore.score;      
                    let played = civScore.count;
                    let average = civScore.average;

                    let civScoreRow = util.createElement('div',['scoreRow']);    
                    let scoreImage = util.createElement('div',['civScoreImage']);
                    scoreImage.style.backgroundImage = 'url("img/civ'+civId+'.jpg")';
                    civScoreRow.append(scoreImage);                
                    civScoreRow.append(util.createElement('div',['scoreName'],civ.name));                    
                    if(this.sortBy === 0) {
                        civScoreRow.append(util.createElement('div',['scoreScore'],average));
                    }
                    if(this.sortBy === 1) {
                        civScoreRow.append(util.createElement('div',['scoreScore'],score));
                    }
                    if(this.sortBy === 2) {
                        civScoreRow.append(util.createElement('div',['scoreScore'],played));
                    }
                    if(this.additionalCivs.includes(civ.id)) {
                        civScoreRow.classList.add('usedAdditional');
                    }
                    civScoreElement.append(civScoreRow);                    
                }                
            }
            
            target.innerHTML = '';            
            let scoreTitle;
            if(this.sortBy === 0) {
                scoreTitle = 'Average Civilization Score';              
            }
            if(this.sortBy === 1) {
                scoreTitle = 'Total Civilization Score';
            }
            if(this.sortBy === 2) {
                scoreTitle = 'Most Popular Civilization';
            }        
            let scoreTitleDiv = util.createElement('div',['scoreTitle'],scoreTitle);
            scoreTitleDiv.append(util.createElement('div',['sortCivArrow'],'sort'));
            target.append(scoreTitleDiv);             
            target.append(civScoreElement);
        }
        
        showHighScores(size, target) {
            let scoreTitle;
            let scoreList = [];
            let nextType = '';
            if(this.scoreType === 0) {
                scoreList = this.monthHigh;
                scoreTitle = '30 days High Scores'; 
                nextType = '7 days';
            }
            if(this.scoreType === 1) {
                scoreList = this.weekHigh;
                scoreTitle = '7 days High Scores';  
                nextType = 'all time';
            }
            if(this.scoreType === 2) {
                scoreList = this.highScores;
                scoreTitle = 'All Time High Scores';  
                nextType = '30 days';
            }
            if(!size) {
                size = scoreList.length +1;
            }
            let playerScoreElement = document.createElement('div');
            playerScoreElement.classList.add('scoreWrapper');            
            for(let [index,playerScore] of scoreList.entries()) {
                let player = playerScore.player;
                let score = playerScore.score;  
                let civs = playerScore.civs.split(',');
                
                let civScoreRow = util.createElement('div',['scoreRow']);
                civScoreRow.append(util.createElement('div',['scoreName'],player));
                civScoreRow.append(util.createElement('div',['scoreScore'],score));
                let civContainer = util.createElement('div',['scoreCivContainer']);
                let usedAdded = false;
                
                for(let usedCiv of civs) {
                    let civ = game.getCivById(parseInt(usedCiv));
                    let scoreImage = util.createElement('div',['civScoreImage']);
                    scoreImage.style.backgroundImage = 'url("img/civ'+civ.id+'.jpg")';
                    scoreImage.setAttribute('title',civ.name);
                    civContainer.append(scoreImage);
                    if(this.additionalCivs.includes(civ.id)) {
                        usedAdded = true;
                    }
                }
                civScoreRow.append(civContainer);
                if(usedAdded) {
                    civScoreRow.classList.add('usedAdditional');
                }
                playerScoreElement.append(civScoreRow);
                if(index >= size) {
                    break;
                }
            }
            
            target.innerHTML = '';
            let scoreTitleDiv = util.createElement('div',['scoreTitle'],scoreTitle);            
            scoreTitleDiv.append(util.createElement('div',['typeCivArrow'],nextType));
            target.append(scoreTitleDiv);
            target.append(playerScoreElement);
        }
        
        showPlayerScores() {
            let playerScoreElement = document.createElement('div');
            playerScoreElement.classList.add('scoreWrapper');            
            for(let playerScore of this.myBestScores) {
                let score = playerScore.score;  
                let civs = playerScore.civs.split(',');
                
                let civScoreRow = util.createElement('div',['scoreRow']);
                civScoreRow.append(util.createElement('div',['scoreScore'],score));
                let civContainer = util.createElement('div',['scoreCivContainer']);
                
                for(let usedCiv of civs) {
                    let civ = game.getCivById(parseInt(usedCiv));
                    let scoreImage = util.createElement('div',['civScoreImage']);
                    scoreImage.style.backgroundImage = 'url("img/civ'+civ.id+'.jpg")';
                    scoreImage.setAttribute('title',civ.name);
                    civContainer.append(scoreImage);
                }
                civScoreRow.append(civContainer);
                playerScoreElement.append(civScoreRow);
            }
            
            qId('myScores').innerHTML = '';
            qId('myScores').append(util.createElement('div',['scoreTitle'],playerName+' Scores'));
            qId('myScores').append(playerScoreElement);
        }
        
        showAchievements() {
            qId('achievementList').append(util.createElement('div',['scoreTitle'],playerName+' Achievements'));
            let achWrapper = util.createElement('div',['achWrapper']);
            achWrapper.append(util.createElement('div',['gridHead'],''));
            achWrapper.append(util.createElement('div',['gridHead'],'Achievement'));
            achWrapper.append(util.createElement('div',['gridHead'],'Requirements'));
            achWrapper.append(util.createElement('div',['gridHead'],'Date achieved'));
            for(let ach of util.achievements) {                
                if(ach.achieved) {
                    achWrapper.append(util.createElement('div',['achDone']));
                }
                else {
                    achWrapper.append(util.createElement('div',['achOpen']));
                }
                achWrapper.append(util.createElement('div',['achName'],ach.name));
                achWrapper.append(util.createElement('div',['achReq'],ach.reqText));
                if(ach.achieved) {
                    achWrapper.append(util.createElement('div',['achWhen'],ach.when));
                }
                else {
                    achWrapper.append(util.createElement('div',['achWhen']));
                }              
            }
            qId('achievementList').append(achWrapper);
        }
        
        toggleAdditionalCivs() {
            if(qId('unofficial').checked) {
                qId('landing').classList.remove('hideAdditional');
            }
            else {
                qId('landing').classList.add('hideAdditional');
            }
        }
    }
    
    class Help {
        constructor() {
            this.civBrowseMarker = 0;
            this.civBrowseSize = 8;
            this.init();
            qId('help').classList.add('visible');            
        }
        
        init() {
            let helpVid = util.createElement('div',['helpVid'],'<iframe width="'+util.baseUnit*11.8+'" height="'+util.baseUnit*8.4+'" src="https://www.youtube.com/embed/QEt6h3Am6F0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');            
            
            let goalText = 'Age of Civilization is played over 6 turns, in which you can use up to 3 civilizations to score the most victory points possible.<br/><br/>';
            goalText += 'Each turn you will use your workers to perform a selection of the actions available for that turn. These actions can grant you one of more of the following:';
            goalText += '<ul><li><div class="icon icon_vp"></div>Victory Points - which determine your final score</li>';
            goalText += '<li><div class="icon icon_coins"></div>Gold - Used to pay for some actions</li>';
            goalText += '<li><div class="icon icon_shield"></div>Military Power - To prevent loss of workers in wars</li>';
            goalText += '<li><div class="icon icon_worker"></div>Worker - Allows you to do an action</li>';
            goalText += '<li><div class="icon icon_wonder"></div>Wonder - Each wonder grants a special bonus</li>';
            goalText += '<li><div class="icon icon_science"></div>Technology - Each technology grants a special bonus</li></ul>';
            
            let civCardText = '<div class="cardExplanation">';            
            civCardText += this.createCardArrow(1.7,1,9,'arrowTextBottom','The amount of workers you will have when rising this civilization. (annexing always grants 1 extra worker)');
            civCardText += this.createCardArrow(2.5,4,1.4,'arrowTextTop','Your main ability while this civilization is active.');
            civCardText += this.createCardArrow(4.9,3.5,2,'arrowTextBottom','A legacy ability that you have the entire game, in this case a discount on the build action.');
            civCardText += '<div class="card civ civ_15" style="background-image: url(img/civ15.jpg);"></div>';
            civCardText += '</div>';
            
            let actionCardText = '<div class="cardExplanation">';
            actionCardText+= '<div class="card action" style="--actionNum:5;"><div class="workerBlock"><div class="changedWorkerArea workerArea singleWorker"></div></div><div class="actionGain1">+<div class="icon icon_coins"></div></div></div>';
            actionCardText += this.createCardArrow(0.6,1.2,6,'arrowTextTop','War happens when this action becomes available. The player with the least <div class="icon icon_shield"></div> loses one <div class="icon icon_worker"></div>.');
            actionCardText += this.createCardArrow(2.95,2.6,3,'arrowTextBottom','What you gain when using this action, in this case 1 gold.');
            actionCardText += this.createCardArrow(5,2.7,3.1,'arrowTextBottom','Each action slot can be used once per round. Some civilization cards contain action slots too.');
            
            let slotHelpText = '<div class="actionSlotExplanation">';
            slotHelpText += 'Action slots can be found on both action and civilization cards.<br/>Each slot can be used once per round, workers placed on it will normally return to you at the end of the round.<br/><br/>';
            slotHelpText += 'Different types of action slots:';
            slotHelpText += '<ul><li><div class="changedWorkerArea workerArea singleWorker"></div> - Normal action slot, requiring a single worker.</li>';
            slotHelpText += '<li><div class="changedWorkerArea workerArea doubleWorker"></div> - Requires two workers to be used.</li>';
            slotHelpText += '<li><div class="changedWorkerArea workerArea singleWorker workerCost_4"></div> - Requires a payment of 4 gold to be used.</li>';
            slotHelpText += '<li><div class="changedWorkerArea workerArea singleWorker sacrificeWorker"></div> - The worker will be sacrificed and won\'t return to you at the end of the round.</li>';
            slotHelpText += '</ul>';
            
            let moreHelpText = '<a href="https://drive.google.com/file/d/1U0J3lvWFknB_IWtXokkzKVDurBZJFoc6/view" target="_blank" rel="noopener">Rulebook v2.3 (English) PDF</a><br/><br/>';
            moreHelpText += '<a href="https://drive.google.com/file/d/1OFlCVCad3VAH8uHbe_4_EaaYIqb0iY-v/view?usp=sharing" target="_blank" rel="noopener">Regel (German) PDF</a><br/><br/>';
            moreHelpText += '<a href="https://drive.google.com/file/d/1tHXQoK904TuURxbYcnTSDdt7EqTjP3op/view?usp=sharing" target="_blank" rel="noopener">Regola (Italian) PDF</a><br/><br/>';
            moreHelpText += '<a href="https://drive.google.com/file/d/1DfMiACIxm1qHrtv-UwfWGSOc7EntO1qL/view?usp=sharing" target="_blank" rel="noopener">Règle (French) PDF</a><br/><br/>';
            moreHelpText += '<a href="https://drive.google.com/file/d/10NsVdF2EEHQsPvoC66SemW0HYNn0xCg6/view?usp=sharing" target="_blank" rel="noopener">reglas (Español) PDF</a><br/><br/>';
            moreHelpText += '<a href="https://boardgamegeek.com/forums/thing/264647/age-civilization" target="_blank">BGG Forum</a><br/><br/>';            
            
            let civBrowserHTML = '<div id="civBrowser"></div>';
            
            q('#help #modalContent').innerHTML = '';
            q('#help #modalContent').append(this.createHelpBlock('Goal of the game',goalText));
            q('#help #modalContent').append(helpVid);
            q('#help #modalContent').append(this.createHelpBlock('Civilization cards',civCardText));
            q('#help #modalContent').append(this.createHelpBlock('Action cards',actionCardText));
            q('#help #modalContent').append(this.createHelpBlock('Action slots',slotHelpText));
            q('#help #modalContent').append(this.createHelpBlock('Useful links',moreHelpText));
            q('#help #modalContent').append(this.createHelpBlock('Civilization Browser',civBrowserHTML));
            
            this.initCivBrowser();
        }
        
        createHelpBlock(title,content) {
            let helpBlock = util.createElement('div',['helpBlock']);
            helpBlock.append(util.createElement('div',['helpBlockTitle'],title));
            helpBlock.append(util.createElement('div',['helpBlockText'],content));
            return helpBlock;
        }
        
        createCardArrow(top,left,arrowLength,textClass,text) {
            let dummy = util.createElement('div',[]);
            let wrapper = util.createElement('div',['cardArrowWrapper']);
            let cardArrow = util.createElement('div',['cardArrow']);
            let cardArrowText = util.createElement('div',['cardArrowText'],text);
            cardArrowText.classList.add(textClass);
            wrapper.style.top = 'calc(var(--baseUnit) *'+top+')';
            wrapper.style.left = 'calc(var(--baseUnit) *'+left+')';
            cardArrow.style.width = 'calc(var(--baseUnit) *'+arrowLength+')';
            wrapper.append(cardArrow);
            wrapper.append(cardArrowText);
            dummy.append(wrapper);
            return dummy.innerHTML;
        }
        
        initCivBrowser() {                        
            qId('civBrowser').append(util.createElement('div', ['civBrowserButton','civBrowseLeft'], '<'));
            qId('civBrowser').append(util.createElement('div', ['civBrowserPanel']));
            qId('civBrowser').append(util.createElement('div', ['civBrowserButton','civBrowseRight'], '>'));
            game.allCivs = game.allCivs.sort((a,b) => (b.name < a.name) ? 1 : -1);
            for(let x=0; x < this.civBrowseSize; x++ ) {
                this.addCivToBrowser(x,true,true);
            }         
            
            let self = this;
            q('.civBrowseLeft').addEventListener( "click", function() {
                self.civBrowseMarker--;
                if(self.civBrowseMarker < 0) {
                    self.civBrowseMarker = game.allCivs.length + self.civBrowseMarker;
                }
                self.addCivToBrowser(self.civBrowseMarker,false,false);
                
            });
            q('.civBrowseRight').addEventListener( "click", function() {
                self.civBrowseMarker++;
                if(self.civBrowseMarker + self.civBrowseSize >= game.allCivs.length) {
                    self.civBrowseMarker = -self.civBrowseSize;
                }
                self.addCivToBrowser(self.civBrowseMarker + self.civBrowseSize,true,false);
                
            });
        }
        
        addCivToBrowser(arrayNum, after, initial) {
            let civId = game.allCivs[arrayNum].id;
            let civObj = game.getCivById(civId);
            let civCard = civObj.show(true);
            if(civObj.unofficial) {
                civCard.classList.add('unofficial');
            }                    
            if(after) {
                q('.civBrowserPanel').append(civCard); 
                if(!initial) {
                    q('.civBrowserPanel .card').remove();
                }
            }
            else {
                q('.civBrowserPanel').prepend(civCard);  
                let allCards = qAll('.civBrowserPanel .card');
                if(!initial) {
                    allCards[allCards.length -1].remove();
                }
            }
        }
    }
    
    var q,qAll,qId,qClass,qTag,playerName;    
    var util = new Util();  
    util.initAchievements();
    var menu = new Menu(); 
    var game = new Game();   
    game.init();
    menu.init();
});