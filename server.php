<?php
    /*
     Copyright (C) 2020 Jan-Dirk van Dingenen

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */
 
require("dbconnect2.inc.php");

if(isset($_POST['player']) && isset($_POST['score']) && isset($_POST['history']) && isset($_POST['civs'])) {
    $player = htmlspecialchars($_POST['player']);
    if($player == '') {
        $player = 'Unknown';
    }
    $score = intval($_POST['score']);
    $civs = $_POST['civs'];
    $his = $_POST['history'];
    $civ1 = 0;
    $civ2 = 0;
    $civ3 = 0;
    $civList = explode(',',$civs);
    array_push($civList,0);
    array_push($civList,0);
    array_push($civList,0);
    
    maak_query("INSERT INTO table_aoc (name, score, history, civs, civ1, civ2, civ3) VALUES(?,?,?,?,?,?,?)",$player,$score,$his,$civs,$civList[0],$civList[1],$civList[2]);
}

if(isset($_GET['monthhighscores'])) {
    $resultList = [];
    $results = maak_query('SELECT name, score, civs FROM table_aoc WHERE deactivated =0 AND created BETWEEN NOW() - INTERVAL 30 DAY AND NOW() ORDER BY score DESC LIMIT 100');
    while ($player = $results->fetch_assoc()) {
        $result = new stdClass;
        $result->player = $player['name'];
        $result->score = $player['score'];
        $result->civs = $player['civs'];
        array_push($resultList,$result);
    }
    echo json_encode($resultList);
}

if(isset($_GET['weekhighscores'])) {
    $resultList = [];
    $results = maak_query('SELECT name, score, civs FROM table_aoc WHERE deactivated =0 AND  created BETWEEN NOW() - INTERVAL 7 DAY AND NOW() ORDER BY score DESC LIMIT 100');
    while ($player = $results->fetch_assoc()) {
        $result = new stdClass;
        $result->player = $player['name'];
        $result->score = $player['score'];
        $result->civs = $player['civs'];
        array_push($resultList,$result);
    }
    echo json_encode($resultList);
}

if(isset($_GET['highscores'])) {
    $resultList = [];
    $results = maak_query('SELECT name, score, civs FROM table_aoc WHERE deactivated =0 ORDER BY score DESC LIMIT 100');
    while ($player = $results->fetch_assoc()) {
        $result = new stdClass;
        $result->player = $player['name'];
        $result->score = $player['score'];
        $result->civs = $player['civs'];
        array_push($resultList,$result);
    }
    echo json_encode($resultList);
}

if(isset($_GET['highcivs'])) {
    $resultList = [];
    $results = maak_query('SELECT civ1, SUM(tot) as total, SUM(rowcount) as totcount FROM (SELECT civ1, SUM(score) AS tot , COUNT(id) as rowcount FROM table_aoc WHERE deactivated = 0 GROUP BY civ1 UNION ALL SELECT civ2, SUM(score) AS tot , COUNT(id) as rowcount FROM table_aoc WHERE deactivated = 0 GROUP BY civ2 UNION ALL SELECT civ3, SUM(score) as tot, COUNT(id) as rowcount FROM table_aoc WHERE deactivated = 0 GROUP BY civ3) as allCivs GROUP BY civ1 ORDER BY TOTAL DESC');
    while ($civ = $results->fetch_assoc()) {
        $result = new stdClass;
        $result->civ = intval($civ['civ1']);
        $result->score = intval($civ['total']);
        $result->count = intval($civ['totcount']);
        array_push($resultList,$result);
    }
    echo json_encode($resultList);
}

if(isset($_GET['mybest'])) {
    $player = htmlspecialchars(urldecode($_GET['mybest']));
    $resultList = [];
    $results = maak_query('SELECT name, score, civs FROM table_aoc WHERE name = ? ORDER BY score DESC LIMIT 100',$player);
    while ($player = $results->fetch_assoc()) {
        $result = new stdClass;
        $result->score = $player['score'];
        $result->civs = $player['civs'];
        array_push($resultList,$result);
    }
    echo json_encode($resultList);
}